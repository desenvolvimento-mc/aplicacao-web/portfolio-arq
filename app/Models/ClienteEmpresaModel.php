<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class ClienteEmpresaModel extends Model
{
    protected $table = 'cliente_empresa';

    protected $allowedFields = [
        'id',
        'cliente_id',
        'empresa_id',
        'situacao',
        'excluido',
        'dt_cadastro',
    ];

    const SITUACAO = [
        1 => 'Ativo',
        2 => 'Inativo'
    ];

    // -- MC 25-08-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            cliente_empresa.id,
            cliente_empresa.dt_cadastro as cliente_empresa_dt_cadastro,
            DATE_FORMAT(cliente_empresa.dt_cadastro, "%d/%m/%Y à\s %H:%i") as cliente_empresa_cadastro,
            cliente_empresa.situacao,

            cliente_empresa.cliente_id,
            cliente.nome as cliente_nome,
            cliente.cpf as cliente_cpf,
            cliente.cel as cliente_cel,
            cliente.email as cliente_email,
            cliente.dt_cadastro as cliente_dt_cadastro,

            cliente_empresa.empresa_id,
            empresa.nome as empresa_nome,
            empresa.cau as empresa_cau,
            empresa.cnpj as empresa_cnpj,
        ')
        ->join('cliente', 'cliente.id = cliente_empresa.cliente_id and cliente.excluido = 0')
        ->join('empresa','empresa.id = cliente_empresa.empresa_id and empresa.excluido = 0')
        ->where('cliente_empresa.excluido',0);

        return $this;
    }

    // -- MC 25-08-2023
    // -- Salva os dados
    public function saveClienteEmpresa($data)
    {
        if ( isset($data['cliente_id']) and isset($data['empresa_id']) ) {
           $findClienteEmpresa = $this
            ->where('cliente_empresa.cliente_id', $data['cliente_id'])
            ->where('cliente_empresa.empresa_id', $data['empresa_id'])
            ->first();

            if ( $findClienteEmpresa ) {
                $data['id'] = $findClienteEmpresa['id'];
            }
        }
        
        return parent::save($data);
    }

    // -- MC 25-08-2023
    // -- Deleta o cliente
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
