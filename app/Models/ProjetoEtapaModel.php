<?php

namespace App\Models;

use CodeIgniter\Model;

class ProjetoEtapaModel extends Model
{
    protected $table = 'projeto_etapa';

    protected $allowedFields = [
        'id',
        'nome',
        'descricao',
        'percentual',
        'situacao',
        'excluido',
        'dt_atualizado',
        'dt_cadastro',
        'dt_finalizado',
        'projeto_id'
    ];

    protected $validationRules = [
        'nome'       => 'required',
        'descricao'  => 'required',
        'percentual' => 'required|integer|is_natural_no_zero',
        'situacao'   => 'required|integer|is_natural_no_zero'
    ];

    protected $validationMessage = [
        'nome'       => ['required' => 'O NOME é obrigatório'],
        'descricao'  => ['required' => 'A DESCRIÇÂO é obrigatória'],
        'percentual' => [
            'required'           => 'O PERCENTUAL é obrigatório',
            'integer'            => 'O PERCENTUAL precisa ser um número inteiro',
            'is_natural_no_zero' => 'O PERCENTUAL precisa ser maior que 0'
        ],
        'situacao'   => [
            'required'           => 'A SITUAÇÃO é obrigatória',
            'integer'            => 'A SITUAÇÃO precisa ser um número inteiro',
            'is_natural_no_zero' => 'A SITUAÇÃO precisa ser maior que 0'
        ]
    ];

    const SITUACAO = [
        1 => 'Em Aberto',
        2 => 'Finalizado'
    ];

    // -- MC 21-08-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            projeto_etapa.id,
            projeto_etapa.nome as etapa_nome,
            projeto_etapa.descricao as etapa_descricao,
            projeto_etapa.percentual as etapa_percentual,
            projeto_etapa.situacao as etapa_situacao,
            projeto_etapa.dt_cadastro as etapa_dt_cadastro,
            projeto_etapa.dt_atualizado as etapa_dt_atualizado,
            projeto_etapa.dt_finalizado as etapa_dt_finalizado,
            DATE_FORMAT(projeto_etapa.dt_cadastro, "%d/%m/%Y %H:%i") as etapa_data_cadastro,
            DATE_FORMAT(projeto_etapa.dt_atualizado, "%d/%m/%Y %H:%i") as etapa_data_atualizado,
            DATE_FORMAT(projeto_etapa.dt_finalizado, "%d/%m/%Y %H:%i") as etapa_data_finalizado,
            IF(
                DATEDIFF(CURRENT_TIMESTAMP, projeto_etapa.dt_cadastro),
                    CONCAT(DATEDIFF(CURRENT_TIMESTAMP, projeto_etapa.dt_cadastro), " dias atrás"),
                    CONCAT(TIME_FORMAT(TIMEDIFF(CURRENT_TIMESTAMP, projeto_etapa.dt_cadastro), "%h"), "h atrás")
            ) as date_time_diff,

            projeto_etapa.projeto_id,
            projeto.titulo as projeto_titulo,
            projeto.descricao as projeto_descricao,
            projeto.endereco as projeto_endereco,
            projeto.prazo_medio as projeto_prazo_medio,
            projeto.prazo_maximo as projeto_prazo_maximo,
            projeto.situacao as projeto_situacao,
            projeto.dt_cadastro as projeto_dt_cadastro,
            DATE_FORMAT(projeto.dt_cadastro, "%d/%m/%Y %H:%i") as projeto_data_cadastro,
        ')
        ->join('projeto', 'projeto.id = projeto_etapa.projeto_id')
        ->where('projeto_etapa.excluido',0);

        return $this;
    }

    // -- MC 21-08-2023
    // -- Salva os dados
    public function saveEtapa($data)
    {
        $Etapa = $this->selectSum('projeto_etapa.percentual')->where('projeto_etapa.projeto_id',$data['projeto_id']);
        if ( isset($data['id']) ) $Etapa->where('projeto_etapa.id !=',$data['id']); 
        $sum = $Etapa->first();

        $sumPercent = $sum['percentual'] + $data['percentual'];

        if ( $sumPercent > 100 ) {
            return ['status'=>false, 'message' => 'O projeto encontra-se finalizado!'];
        }

        $data['dt_finalizado'] = $data['situacao'] == 2 ? date("Y-m-d H:i:s") : null;

        $res = parent::save($data);

        if ( !$res ) return ['status' => false];

        $id = !isset($data['id']) ? $this->getInsertID() : $data['id'];

        $getProjetoPercent = $this
        ->selectSum('projeto_etapa.percentual')
        ->where('projeto_etapa.projeto_id', $data['projeto_id'])
        ->first();

        $Projeto = new ProjetoModel();
        $projeto = $Projeto->find($data['projeto_id']);

        if ( $getProjetoPercent['percentual'] == 100 and $projeto['situacao'] != 3 ) {
            $editProjetoSituacao = $Projeto->save(['situacao' => 3, 'id' => $data['projeto_id']]);
            if ( $editProjetoSituacao )
                $projeto['situacao'] = 3;
        } else if ( $projeto['situacao'] != 1 ) {
            $editProjetoSituacao = $Projeto->save(['situacao' => 2, 'id' => $data['projeto_id']]);
            if ( $editProjetoSituacao )
                $projeto['situacao'] = 2;
        }

        return ['status' => true, 'projeto_situacao' => $projeto['situacao']];
    }

    // -- MC 21-08-2023
    // -- Deleta a etapa do projeto
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
