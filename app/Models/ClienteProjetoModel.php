<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class ClienteProjetoModel extends Model
{
    protected $table = 'cliente_projeto';

    protected $allowedFields = [
        'id',
        'cliente_empresa_id',
        'projeto_id',
        'situacao',
        'excluido',
        'dt_cadastro',
    ];

    // -- MC 25-08-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            cliente_projeto.id,
            cliente_projeto.dt_cadastro as cliente_projeto_dt_cadastro,
            DATE_FORMAT(cliente_projeto.dt_cadastro, "%d/%m/%Y à\s %H:%i") as cliente_projeto_cadastro,

            cliente_projeto.cliente_empresa_id,
            cliente.nome as cliente_nome,
            cliente.cpf as cliente_cpf,
            cliente.cel as cliente_cel,
            cliente.email as cliente_email,
            cliente.dt_cadastro as cliente_dt_cadastro,
        ')
        ->join('cliente_empresa', 'cliente_empresa.id = cliente_projeto.cliente_empresa_id')
        ->join('cliente', 'cliente.id = cliente_empresa.cliente_id and cliente.excluido = 0')
        ->where('cliente_projeto.excluido', 0);

        return $this;
    }

    // -- MC 25-08-2023
    // -- Salva os dados
    public function saveClienteProjeto($data)
    {
        $res = true;
        $projeto_id = $data['id'];

        $ClienteProjeto = $this->where('cliente_projeto.projeto_id', $projeto_id)->where('cliente_projeto.excluido', 0);
        if ( isset($data['cliente_empresa_id']) )
        $ClienteProjeto->whereNotIn('cliente_projeto.cliente_empresa_id', $data['cliente_empresa_id']);
        
        $updateClienteProjeto = $ClienteProjeto->findAll();

        if ( $updateClienteProjeto ) {
            $res = array_map(function ($value) {
                $update['id'] = $value['id'];
                $update['excluido'] = 1;
                return parent::save($update);
            }, $updateClienteProjeto);
        }
        
        if ( isset($data['cliente_empresa_id']) ) {
            $res = array_map(function ($value) use ($projeto_id) {
                $clienteProjeto = [];
                $findClienteProjeto = $this->where('cliente_projeto.cliente_empresa_id', $value)->where('cliente_projeto.projeto_id', $projeto_id)->first();
                if (!$findClienteProjeto) {
                    $clienteProjeto['cliente_empresa_id'] = $value;
                    $clienteProjeto['projeto_id'] = $projeto_id;
                } else if ($findClienteProjeto['excluido'] != 0) {
                    $clienteProjeto['id'] = $findClienteProjeto['id'];
                    $clienteProjeto['excluido'] = 0;
                }
                return parent::save($clienteProjeto);
            }, $data['cliente_empresa_id']);
        }
        
        if ( $res ) return true;

        return false;
    }

    // -- MC 25-08-2023
    // -- Deleta o cliente
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }
}
