<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class UsuarioModel extends Model
{
    protected $table = 'usuario';

    protected $allowedFields = [
        'id',
        'nome',
        'cel',
        'email',
        'senha',
        'acesso',
        'permissao',
        'situacao',
        'dt_final_acesso',
        'excluido',
        'dt_cadastro',
        'empresa_id'
    ];

    protected $validationRules = [
        'nome'       => 'required',
        'cel'        => 'required|numeric|min_length[11]|max_length[11]',
        'email'      => 'required|valid_email',
        'senha'      => 'required',
        'conf_senha' => 'required',
        'situacao'   => 'required|integer|is_natural_no_zero',
        'acesso'     => 'required|integer|is_natural_no_zero',
    ];

    protected $validationMessage = [
        'nome' => ['required' => 'O NOME é obrigatório'],
        'cel' => [
            'required'   => 'O CELULAR é obrigatório',
            'numeric'    => 'O CELULAR está inválido',
            'min_length' => 'O CELULAR precisa ter 11 números',
            'max_length' => 'O CELULAR precisa ter 11 números'
        ],
        'email' => [
            'required' => 'O EMAIL é obrigatório',
            'valid_email' => 'O EMAIL está inválido'
        ],
        'senha' => ['required' => 'A SENHA é obrigatório'],
        'conf_senha' => ['required' => 'É necessário CONFIRMAR A SENHA'],
        'situacao' => [
            'required'           => 'A SITUAÇÃO é obrigatória',
            'integer'            => 'A SITUAÇÃO precisa ser um número inteiro',
            'is_natural_no_zero' => 'A SITUAÇÃO precisa ser maior que 0'
        ],
        'acesso' => [
            'required'           => 'O ACESSO é obrigatório',
            'integer'            => 'O ACESSO precisa ser um número inteiro',
            'is_natural_no_zero' => 'O ACESSO precisa ser maior que 0'
        ],
    ];

    const ACESSO = [
        1 => 'Funcionário',
        2 => 'Temporário',
        3 => 'Cliente'
    ];

    const SITUACAO = [
        1 => 'Ativo',
        2 => 'Inativo'
    ];

    // -- MC 25-06-2023
    // -- Criptografa a senha
    public function passGenerate($senha)
    {
        return md5("skshdskihdl\ajdi1d54sd".$senha."jilçjfijgr45f64df5465sd");
    }

    // -- MC 24-06-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            usuario.id,
            usuario.nome,
            usuario.cel,
            usuario.email,
            usuario.senha,
            usuario.acesso,
            usuario.permissao,
            usuario.situacao,
            usuario.dt_cadastro,
            DATE_FORMAT(usuario.dt_cadastro, "%d/%m/%Y à\s %H:%i") as data_cadastro,

            usuario.empresa_id,
            empresa.nome as empresa_nome,
            empresa.cau as empresa_cau,
            empresa.cnpj as empresa_cnpj,
        ')
        ->join('empresa','empresa.id = usuario.empresa_id','left')
        ->where('usuario.excluido',0);

        return $this;
    }

    // -- MC 24-06-2023
    // -- Salva os dados
    public function saveUsuario($data)
    {
        if ( !empty($data['senha']) ) {
            $data['senha'] = $this->passGenerate($data['senha']);
        
        } else unset($data['senha']);

        $data['empresa_id'] = Auth::getData('empresa_id');
        
        return parent::save($data);
    }

    //  -- MC 24-08-2023
    // -- Realiza o login do usuário no sistema
    public function login($data)
    {
        $dataUsuario = $this->where("usuario.email", $data['email'])->first();
        $senha = $this->passGenerate($data['senha']);

        if ( !$dataUsuario or $senha != $dataUsuario['senha'] ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Email ou senha incorreto!']);
            return false;
        } else {
            if ( $dataUsuario['situacao'] != 1 ) {
                session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Seu cadastro encontra-se desativado. Por favor, entre em contato com a administração do sistema!']);
                return false;
            } else {
                $usuario = $this->prepare()->find($dataUsuario['id']);
                Auth::setData($usuario);
                return true;
            }
        }
    }

    // -- MC 24-06-2023
    // -- Deleta o usuario
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }
}
