<?php

namespace App\Models;

use CodeIgniter\Model;

class ProjetoAnexoModel extends Model
{
    protected $table = 'projeto_anexo';

    protected $allowedFields = [
        'id',
        'titulo',
        'descricao',
        'anexo',
        'tipo_anexo',
        'excluido',
        'dt_cadastro',
        'projeto_id',
        'projeto_etapa_id'
    ];

    private $PATH = WRITEPATH.'projetos/';

    const TIPO = [
        'image/jpeg',
        'image/png',
        'application/pdf'
    ];

    const TIPO_IMG = [
        'image/jpeg',
        'image/png'
    ];

    // -- MC 21-08-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            projeto_anexo.id,
            projeto_anexo.titulo,
            projeto_anexo.descricao,
            projeto_anexo.anexo,
            projeto_anexo.tipo_anexo,
            DATE_FORMAT(projeto_anexo.dt_cadastro, "%d/%m/%Y %H:%i") as anexo_data_cadastro,

            projeto_anexo.projeto_id,
            projeto.titulo as projeto_titulo,
            projeto.descricao as projeto_descricao,
            projeto.endereco as projeto_endereco,
            projeto.prazo_medio as projeto_prazo_medio,
            projeto.prazo_maximo as projeto_prazo_maximo,
            projeto.situacao as projeto_situacao,
            projeto.dt_cadastro as projeto_dt_cadastro,
            DATE_FORMAT(projeto.dt_cadastro, "%d/%m/%Y à\s %H:%i") as projeto_data_cadastro,

            projeto_anexo.projeto_etapa_id
        ')
        ->join('projeto', 'projeto.id = projeto_anexo.projeto_id')
        ->where('projeto_anexo.excluido',0);

        return $this;
    }

    // -- MC 21-08-2023
    // -- Salva os dados
    public function saveAnexo($data)
    {
        if ( isset($data['anexo']) and !empty($data['anexo']) ) {
            $data['tipo_anexo'] = $data['anexo']->getMimeType();
            $name = $data['anexo']->getName();

            $i = 1;
            while ( file_exists($this->PATH.$data['projeto_id'].'/anexos/'.$name) ) {
                $name = '('.$i.')'.$name;
                $i++;
            }

            $res = $data['anexo']->move($this->PATH.$data['projeto_id'].'/anexos', $name);
            if ( !$res ) return false;
        }

        if ( isset($name) ) $data['anexo'] = $name;
        
        return parent::save($data);
    }

    // -- MC 21-08-2023
    // -- Deleta a etapa do projeto
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
