<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class EmpresaModel extends Model
{
    protected $table = 'empresa';

    protected $allowedFields = [
        'id',
        'nome',
        'cau',
        'cnpj',
        'cel',
        'email',
        'descricao',
        'endereco',
        'foto',
        'excluido',
        'dt_cadastro'
    ];

    // -- MC 24-06-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this->select(
            'empresa.id',
            'empresa.nome',
            'empresa.cau',
            'empresa.cnpj',
            'empresa.cel',
            'empresa.email',
            'empresa.descricao',
            'empresa.foto',
            'empresa.excluido',
            'empresa.dt_cadastro'
        );

        return $this;
    }

    // -- MC 24-06-2023
    // -- Salva os dados
    public function saveEmpresa($data)
    {
        return parent::save($data);
    }

    // -- MC 24-06-2023
    // -- Deleta a empresa
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
