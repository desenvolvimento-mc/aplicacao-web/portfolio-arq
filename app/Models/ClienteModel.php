<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class ClienteModel extends Model
{
    protected $table = 'cliente';

    protected $allowedFields = [
        'id',
        'nome',
        'cpf',
        'cel',
        'email',
        'excluido',
        'dt_cadastro'
    ];

    protected $validationRules = [
        'nome'  => 'required',
        'cpf'   => 'required|numeric|min_length[11]|max_length[11]',
        'cel'   => 'required|numeric|min_length[11]|max_length[11]',
        'email' => 'required|valid_email'
    ];

    protected $validationMessage = [
        'nome' => ['required' => 'O NOME é obrigatório'],
        'cpf' => [
            'required'   => 'O CPF é obrigatório',
            'numeric'    => 'O CPF está inválido',
            'min_length' => 'O CPF precisa ter 11 números',
            'max_length' => 'O CPF precisa ter 11 números'
        ],
        'cel' => [
            'required'   => 'O CELULAR é obrigatório',
            'numeric'    => 'O CELULAR está inválido',
            'min_length' => 'O CELULAR precisa ter 11 números',
            'max_length' => 'O CELULAR precisa ter 11 números'
        ],
        'email' => [
            'required' => 'O EMAIL é obrigatório',
            'valid_email' => 'O EMAIL está inválido'
        ],
    ];

    // -- MC 25-06-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            cliente.id,
            cliente.nome,
            cliente.cpf,
            cliente.cel,
            cliente.email,
            cliente.dt_cadastro,
            DATE_FORMAT(cliente.dt_cadastro, "%d/%m/%Y à\s %H:%i") as data_cadastro,
        ')
        ->where('cliente.excluido',0);

        return $this;
    }

    // -- MC 25-06-2023
    // -- Salva os dados
    public function saveCliente($data)
    {
        $dataClienteEmpresa['situacao'] = $data['situacao'];
        unset($data['situacao']);

        if ( isset($data['cliente_empresa_id']) ) {
            $dataClienteEmpresa['id'] = $data['cliente_empresa_id'];
            unset($data['cliente_empresa_id']);
        }
        
        if ( isset($data['cpf']) and !empty($data['cpf']) ) {
            $findClienteCpf = $this->where('cliente.cpf', $data['cpf'])->first();
            if ( $findClienteCpf ) {
                $data['id'] = $findClienteCpf['id'];
            }
        }

        $res = parent::save($data);

        if ( !$res ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados do cliente! Por favor, tente mais tarde.']);
            return false;
        }

        $id = isset($data['id']) ? $data['id'] : $this->getInsertID();

        $ClienteEmpresa = new ClienteEmpresaModel();
        $dataClienteEmpresa['empresa_id'] = Auth::getData('empresa_id');
        $dataClienteEmpresa['cliente_id'] = $id;

        if ( !$ClienteEmpresa->saveClienteEmpresa($dataClienteEmpresa) ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados do cliente! Por favor, tente mais tarde.']);
            return false;
        }

        return true;
    }

    // -- MC 25-06-2023
    // -- Deleta o cliente
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
