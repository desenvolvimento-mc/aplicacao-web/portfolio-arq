<?php

namespace App\Models;

use CodeIgniter\Model;

class HonorarioModel extends Model
{
    protected $table = 'honorario';

    protected $allowedFields = [
        'id',
        'nome',
        'valor',
        'descricao',
        'dt_cadastro',
        'excluido',
        'projeto_id'
    ];


    // -- MC 23-07-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            honorario.id,
            honorario.nome,
            honorario.valor,
            honorario.descricao,
            DATE_FORMAT(honorario.dt_cadastro, "%d/%m/%Y %H:%i") as data_cadastro,
            honorario.projeto_id
        ')
        ->where('honorario.excluido', 0);

        return $this;
    }

    // -- MC 23-07-2023
    // -- Deleta o honorario
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }
}
