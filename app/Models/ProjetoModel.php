<?php

namespace App\Models;

use App\Auth;
use CodeIgniter\Model;

class ProjetoModel extends Model
{
    protected $table = 'projeto';

    protected $allowedFields = [
        'id',
        'titulo',
        'descricao',
        'endereco',
        'prazo_medio',
        'prazo_maximo',
        'situacao',
        'excluido',
        'dt_cadastro',
        'usuario_id',
        'cliente_id'
    ];

    const SITUACAO = [
        1 => 'Ativo',
        2 => 'Em Andamento',
        3 => 'Finalizado'
    ];

    // -- MC 25-06-2023
    // -- Prepara os dados
    public function prepare()
    {
        $this
        ->select('
            projeto.id,
            projeto.titulo,
            CONCAT(SUBSTR(projeto.titulo, 1, 40), "...") as projeto_titulo,
            projeto.descricao,
            projeto.endereco,
            projeto.prazo_medio,
            projeto.prazo_maximo,
            projeto.situacao,
            projeto.dt_cadastro,
            DATE_FORMAT(projeto.dt_cadastro, "%d/%m/%Y à\s %H:%i") as data_cadastro,

            IF(
                DATEDIFF(CURRENT_TIMESTAMP, projeto.dt_cadastro),
                    CONCAT(DATEDIFF(CURRENT_TIMESTAMP, projeto.dt_cadastro), " dias"),
                    CONCAT(TIME_FORMAT(TIMEDIFF(CURRENT_TIMESTAMP, projeto.dt_cadastro), "%h"), " horas")
            ) as date_time_diff,

            projeto.usuario_id,
            usuario.nome as usuario_nome,
            usuario.cel as usuario_cel,
            usuario.email as usuario_email,

            usuario.empresa_id,
            empresa.nome as empresa_nome,
            empresa.cau as empresa_cau,
            empresa.cnpj as empresa_cnpj,

            IFNULL(SUM(projeto_etapa.percentual), "0") as percentual
        ')
        ->join('usuario', 'usuario.id = projeto.usuario_id')
        ->join('empresa','empresa.id = usuario.empresa_id','left')
        ->join('projeto_etapa', 'projeto_etapa.projeto_id = projeto.id','left')
        ->groupBy('projeto.id')
        ->where('projeto.excluido',0);

        return $this;
    }

    // -- MC 25-06-2023
    // -- Salva os dados
    public function saveProjeto($data)
    {
        $data['usuario_id'] = Auth::getData('id');

        if ( !isset($data['situacao']) ) $data['situacao'] = 1;

        if ( isset($data['cliente_empresa_id']) ) {
            $dataClienteProjeto['cliente_empresa_id'] = $data['cliente_empresa_id'];
            unset($data['cliente_empresa_id']);
        }

        $res = parent::save($data);
        
        if ( $res ) {
            $dataClienteProjeto['id'] = isset($data['id']) ? $data['id'] : $this->getInsertID();
            $ClienteProjeto = new ClienteProjetoModel();
            if ( $ClienteProjeto->saveClienteProjeto($dataClienteProjeto) ) 
                return true;
        }

        session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Erro ao salvar os dados do projeto!']);
        return false;
    }

    // -- MC 25-06-2023
    // -- Deleta o projeto
    public function deleteId($id)
    {
        $data = ['excluido' => 1, 'id' => $id];

        return parent::save($data);
    }

}
