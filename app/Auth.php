<?php

namespace App;

use App\Models\UsuarioModel;

class Auth
{
    static private $session = 'USERAUTH';

    // -- Cria a session com os dados do usuário
    static public function setData($data)
    {
        session()->set(static::$session, $data);
    }

    // -- Recupera da session os dados do usuário logado
    // -- Traz o array com todos os dados do usuário como também campos específicos passados como parâmetro para a função
    static public function getData($value = null)
    {
        $dados = session()->get(static::$session);
        if ($value) {
            if (isset($dados[$value])) return $dados[$value];
            else return false;
        }
        return $dados;
    }

    // -- Atualiza os dados do usuário logado na session
    static public function refreshData()
    {
        $dados = session()->get(static::$session);
        if ($dados) {
            $Usuario = new UsuarioModel();
            $usuario = $Usuario->prepare()->find($dados['id']);
            if ($usuario) {
                session()->set(static::$session, $usuario);
            }
        }
        return false;
    }

    // -- Realiza o logout, nesse caso remove da session o usuário
    static public function logout()
    {
        return session()->remove(static::$session);
    }

    // -- Realiza as verificações de permissões do usuário logado
    // -- Retorna true ou false de acordo com o tipo de acesso do usuário
    // -- Retorna false caso seja o primeiro acesso do usuário para que a senha seja trocada.
    static public function acesso($permissao = [])
    {
        $dados = session()->get(static::$session);
        if ( $dados ) {
            if ( !$permissao ) return true;
            if ( in_array($dados['permissao'], $permissao) ) return true;
        }
        return false;
    }

}
