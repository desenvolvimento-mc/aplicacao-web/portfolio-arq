<script src="<?= base_url('assets/libs/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/bootstrap/dist/js/bootstrap.min.js') ?> "></script>
<!-- apps -->
<!-- apps -->
<script src="<?= base_url('dist/js/app-style-switcher.js') ?> "></script>
<script src="<?= base_url('dist/js/feather.min.js') ?> "></script>
<script src="<?= base_url('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') ?> "></script>
<script src="<?= base_url('dist/js/sidebarmenu.js') ?> "></script>
<!--Custom JavaScript -->
<script src="<?= base_url('dist/js/custom.min.js') ?>"></script>
<!--This page JavaScript -->
<script src="<?= base_url('assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js') ?>"></script>
<script src="<?= base_url('assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js') ?>"></script>

<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script> -->

<script src="<?= base_url('sistema/js/admin.js') ?>"></script>

<?= $this->renderSection('javascript') ?>