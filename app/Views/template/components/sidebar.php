<ul id="sidebarnav">
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="<?= site_url('/') ?>" aria-expanded="false">
            <i data-feather="home" class="feather-icon"></i>
            <span class="hide-menu">Home</span>
        </a>
    </li>
    <li class="list-divider"></li>
    <li class="nav-small-cap"><span class="hide-menu">Sistema</span></li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="<?= site_url('usuario') ?>" aria-expanded="false">
            <i data-feather="home" class="feather-icon"></i>
            <span class="hide-menu">Usuários</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="<?= site_url('cliente') ?>" aria-expanded="false">
            <i data-feather="home" class="feather-icon"></i>
            <span class="hide-menu">Clientes</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="<?= site_url('projeto') ?>" aria-expanded="false">
            <i data-feather="home" class="feather-icon"></i>
            <span class="hide-menu">Projetos</span>
        </a>
    </li>
    <li class="list-divider"></li>
</ul>