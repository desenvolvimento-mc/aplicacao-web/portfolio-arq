<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?= $this->include('template/components/head') ?>
<!-- /.Head -->

<body>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- Header -->
        <?= $this->include('template/components/header') ?>
        <!-- /.Header -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <nav class="sidebar-nav">
                    <?= $this->include('template/components/sidebar') ?>
                </nav>
            </div>
        </aside>

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <?= $this->renderSection('breadcrumb') ?>
                </div>
            </div>

            <div class="container-fluid">
                <?= $this->include('template/flash') ?>
                <?= $this->renderSection('content') ?>
            </div>
            <footer class="footer text-center text-muted">
                Desenvolvido por <a href="https://gitlab.com/mclaradiascampos">Maria Clara Dias</a>.
            </footer>
        </div>
    </div>
    <!-- javascript -->
    <?= $this->include('template/components/javascript') ?>
    <!-- /.javascript -->
</body>

</html>