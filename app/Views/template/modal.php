<div id="modalBackdrop" class="modal-backdrop fade" style="display: none;"></div>
<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalLabel">Alert Model</h5>
				<button type="button" class="btn btn-light rounded-circle" aria-label="Fechar" onclick="modal.hide()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modalMessage">
				Lorem ipsum dolor sit amet...
			</div>
			<div class="modal-footer" id="modalButton"></div>
		</div>
	</div>
</div>