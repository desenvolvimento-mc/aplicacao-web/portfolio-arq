<?php
$session = session()->getFlashdata('flash');
?>

<?php if (isset($session) and !empty($session)) : ?>
    <div class="alert alert-<?=isset($session['class']) ? $session['class'] : 'info' ?> fade show" role="alert">
        <?= isset($session['message']) ? $session['message'] : '' ?>
    </div>
<?php endif; ?>