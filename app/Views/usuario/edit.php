<?php
$acessos = App\Models\UsuarioModel::ACESSO;
$situacoes = \App\Models\UsuarioModel::SITUACAO;
?>

<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-7 align-self-center">
    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Usuários</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0 p-0">
                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= site_url('usuario') ?>">Usuários</a></li>
                <li class="breadcrumb-item text-muted active">Editar</li>
            </ol>
        </nav>
    </div>
</div>
<div class="col-5 align-self-center">
    <div class="customize-input float-right">
        <a href="<?= site_url('usuario') ?>" class="btn btn-rounded btn-secondary pull-right">
            Listar
        </a>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?= form_open('usuario/edit/' . $dados['id']) ?>
                <form action="">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control <?= validation_show_error('nome') ? 'is-invalid' : '' ?>" id="nome" name="nome" value="<?= $dados['nome'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('nome') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cel">Celular</label>
                                <input type="tel" class="form-control <?= validation_show_error('cel') ? 'is-invalid' : '' ?>" id="cel" name="cel" value="<?= $dados['cel'] ?>" required minlength="11" maxlength="11">
                                <small class="text-danger"><?= validation_show_error('cel') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control <?= validation_show_error('email') ? 'is-invalid' : '' ?>" id="email" name="email" value="<?= $dados['email'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('email') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="password" class="form-control <?= validation_show_error('senha') ? 'is-invalid' : '' ?>" id="senha" name="senha">
                                <small class="text-danger"><?= validation_show_error('senha') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="conf_senha">Confirmar Senha</label>
                                <input type="password" class="form-control <?= validation_show_error('conf_senha') ? 'is-invalid' : '' ?>" id="conf_senha" name="conf_senha">
                                <small class="text-danger"><?= validation_show_error('conf_senha') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="situacao">Situação</label>
                                <select class="form-control <?= validation_show_error('situacao') ? 'is-invalid' : '' ?>" name="situacao" id="situacao" required>
                                    <?php foreach ($situacoes as $key => $situacao) : ?>
                                        <option value="<?= $key ?>"><?= $situacao ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="text-danger"><?= validation_show_error('situacao') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="acesso">Acesso</label>
                                <select class="form-control <?= validation_show_error('acesso') ? 'is-invalid' : '' ?>" name="acesso" id="acesso" required>
                                    <?php foreach ($acessos as $key => $acesso) : ?>
                                        <option value="<?= $key ?>" <?= $dados['acesso'] == $key ? 'selected' : '' ?>><?= $acesso ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="text-danger"><?= validation_show_error('acesso') ?></small>
                            </div>
                        </div>
                        <?php if ($dados['acesso'] == 2) : ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="dt_final_acesso">Data Final</label>
                                    <input type="date" class="form-control" name="dt_final_acesso" id="dt_final_acesso" value="<?=$dados['dt_final_acesso']?>" required>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <input type="hidden" name="id" value="<?= $dados['id'] ?>">
                    <button type="submit" class="btn btn-secondary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {
        let acesso = document.querySelector('select[name="acesso"]');

        showDtFinal = (ev) => {
            let dt_final = document.querySelector('input[name="dt_final_acesso"]');
            let div = ev.target.closest('div[class="row"]');

            if (acesso.value == 2) {
                let div_dt = document.createElement('div');
                div_dt.className = "col-md-4";
                div_dt.innerHTML = `
                <div class="form-group">
                <label for="dt_final_acesso">Data Final</label>
                <input type="date" class="form-control" id="dt_final_acesso" name="dt_final_acesso" required>
                </div>`;

                div.appendChild(div_dt);
            } else {
                dt_final?.parentNode.parentNode.remove();
            }
        }

        acesso.addEventListener('change', (ev) => {
            this.showDtFinal(ev);
        })
    });
</script>
<?= $this->endSection() ?>