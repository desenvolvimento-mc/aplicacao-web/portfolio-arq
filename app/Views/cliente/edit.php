<?php
$situacoes = \App\Models\ClienteEmpresaModel::SITUACAO;
?>

<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-7 align-self-center">
    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Clientes</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0 p-0">
                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="<?= site_url('cliente') ?>">Clientes</a></li>
                <li class="breadcrumb-item text-muted active">Editar</li>
            </ol>
        </nav>
    </div>
</div>
<div class="col-5 align-self-center">
    <div class="customize-input float-right">
        <a href="<?= site_url('cliente') ?>" class="btn btn-rounded btn-secondary pull-right">
            Listar
        </a>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <?= form_open('cliente/add') ?>
                <form action="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control <?= validation_show_error('nome') ? 'is-invalid' : '' ?>" id="nome" name="nome" value="<?= $dados['cliente_nome'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('nome') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control <?= validation_show_error('cpf') ? 'is-invalid' : '' ?>" id="cpf" name="cpf" minlength="11" maxlength="11" value="<?= $dados['cliente_cpf'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('cpf') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cel">Celular</label>
                                <input type="tel" class="form-control <?= validation_show_error('cel') ? 'is-invalid' : '' ?>" id="cel" name="cel" minlength="11" maxlength="11" value="<?= $dados['cliente_cel'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('cel') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control <?= validation_show_error('email') ? 'is-invalid' : '' ?>" id="email" name="email" value="<?= $dados['cliente_email'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('email') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="situacao">Situação</label>
                                <select class="form-control <?= validation_show_error('situacao') ? 'is-invalid' : '' ?>" name="situacao" id="situacao">
                                    <?php foreach ($situacoes as $key => $situacao) : ?>
                                        <option value="<?= $key ?>" <?= $dados['situacao'] == $key ? 'selected' : '' ?>><?= $situacao ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="text-danger"><?= validation_show_error('situacao') ?></small>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $dados['cliente_id'] ?>">
                    <input type="hidden" name="cliente_empresa_id" value="<?= $dados['id'] ?>">
                    <button type="submit" class="btn btn-secondary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>