<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('style') ?>
<link href="<?= base_url('sistema/projeto/css/theme.min.css') ?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

<style>
    .page-wrapper {
        background: transparent !important;
        padding: 0;
    }

    .page-wrapper>.container-fluid {
        padding: 25px;
        min-height: calc(100vh - 210px);
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-md-12">
        <div class="bg-success-soft mt-n6 pt-6 mb-6" style="background: #d6d6d6 !important;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="d-lg-flex align-items-center justify-content-between mb-6 px-6">
                        <div class="mb-6 mb-lg-0">
                            <h1 class="mb-0 h3 "><?= $projeto['titulo'] ?></h1>
                            <span>
                                <span class="fs-6">
                                    <span class="me-1"><i class="mdi mdi-domain fs-5 me-1"></i>Maria Clara Dias Campos</span> |
                                    <span class="mx-1">Início:
                                        <span class="text-dark"><?= $projeto['data_cadastro'] ?></span>
                                    </span> |
                                    <span class="mx-1">Situação:
                                        <span class="text-dark"><?= App\Models\ProjetoModel::SITUACAO[$projeto['situacao']] ?></span>
                                    </span>
                                </span>
                            </span>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="avatar-group">
                                <?php foreach ($projeto['clientes'] as $key => $cliente) : ?>
                                    <span class="avatar avatar-md">
                                        <img alt="avatar" src="<?= base_url('assets/images/users/1.jpg') ?>" class="rounded-circle imgtooltip" data-toggle="tooltip" data-placement="bottom" title="<?= $cliente['cliente_nome'] ?>">
                                    </span>
                                <?php endforeach; ?>
                                <span class="avatar avatar-md">
                                    <img alt="avatar" src="<?= base_url('assets/images/users/2.jpg') ?>" class="rounded-circle imgtooltip" data-toggle="tooltip" data-placement="bottom" title="<?= $projeto['usuario_nome'] ?>">
                                </span>
                            </div>
                            <a href=" #!" class="btn btn-icon btn-white border border-2 rounded-circle btn-dashed ms-2" data-template="inviteMember" data-bs-toggle="modal" data-bs-target="#inviteMemberModal">+</a>
                        </div>
                    </div>
                    <div class="col-12">
                        <ul class="nav nav-lb-tab px-6">
                            <li class="nav-item ms-0 me-3">
                                <a href="#projeto" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                    <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                                    <span class="d-none d-lg-block">Projeto</span>
                                </a>
                            </li>
                            <li class="nav-item mx-3">
                                <a href="#etapa" data-toggle="tab" aria-expanded="true" class="nav-link">
                                    <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                                    <span class="d-none d-lg-block">Etapas</span>
                                </a>
                            </li>
                            <li class="nav-item mx-3">
                                <a href="#anexo" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
                                    <span class="d-none d-lg-block">Anexos</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <?php $view = \Config\Services::renderer(); ?>
        <div class="tab-pane active" id="projeto">
            <?= $view->setVar('projeto', $projeto)->render('projeto/partials/projeto'); ?>
        </div>
        <div class="tab-pane show" id="etapa">
            <?= $view->setVar('projeto', $projeto)->render('projeto/partials/etapa'); ?>
        </div>
        <div class="tab-pane" id="anexo">
            <?= $view->setVar('projeto', $projeto)->render('projeto/partials/anexo'); ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script src="<?= base_url('sistema/projeto/js/theme.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?= $this->endSection() ?>