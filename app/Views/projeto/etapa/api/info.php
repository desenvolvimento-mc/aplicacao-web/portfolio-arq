<?php
$situacoes = \App\Models\ProjetoEtapaModel::SITUACAO;
?>

<ul class="nav nav-tabs nav-justified nav-bordered">
    <li class="nav-item ms-0 me-3">
        <a href="#etapa_info" data-toggle="tab" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">Informações da Etapa</span>
        </a>
    </li>
    <li class="nav-item mx-3">
        <a href="#etapa_anexo" data-toggle="tab" aria-expanded="true" class="nav-link">
            <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">Anexos</span>
        </a>
    </li>
    <li class="nav-item mx-3">
        <a href="#etapa_comentario" data-toggle="tab" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">Comentários</span>
        </a>
    </li>
</ul>

<div class="tab-content mt-3 p-2">
    <div class="tab-pane active" id="etapa_info">
        <form class="row">
            <div class="col-md-12 mb-2">
                <label for="nome">Nome</label>
                <input type="text" class="form-control bg-white" name="nome" value="<?= $etapa['etapa_nome'] ?>" disabled>
            </div>
            <div class="col-md-6 mb-2">
                <label for="situacao">Situação</label>
                <select class="form-select bg-white" name="situacao" id="situacao" disabled>
                    <?php foreach ($situacoes as $key => $situacao) : ?>
                        <option value="<?= $key ?>" <?= $key == $etapa['etapa_situacao'] ? 'selected' : '' ?>><?= $situacao ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-6 mb-2">
                <label for="nome">Percentual (%)</label>
                <input type="number" min="1" max="100" class="form-control bg-white" name="percentual" value="<?= $etapa['etapa_percentual'] ?>" disabled>
            </div>
            <div class="col-md-6 mb-2">
                <label for="nome">Cadastro</label>
                <input type="text" class="form-control bg-white" id="dt_cadastro" value="<?= $etapa['etapa_data_cadastro'] ?>" disabled>
            </div>
            <div class="col-md-6 mb-2">
                <label for="nome">Última Atualização</label>
                <input type="text" class="form-control bg-white" id="dt_atualizado" value="<?= $etapa['etapa_data_atualizado'] ?>" disabled>
            </div>
            <div class="col-md-12 mb-2">
                <label for="nome">Descrição</label>
                <textarea class="form-control bg-white" name="descricao" disabled><?= $etapa['etapa_descricao'] ?></textarea>
            </div>
            <div class="col-md-12">
                <div class="float-end">
                    <button type="button" class="btn btn-sm btn-secondary" id="edit-etapa" onclick="editEtapa(event)">Editar Etapa</button>
                </div>
            </div>
            <input type="hidden" name="id" value="<?= $etapa['id'] ?>">
            <input type="hidden" name="projeto_id" value="<?= $etapa['projeto_id'] ?>">
        </form>
    </div>
    <div class="tab-pane show" id="etapa_anexo">
        <div class="row">
            <div class="col-md-12 mb-2">
                <div class="float-start">
                    <button class="btn btn-sm btn-secondary" id="edit-etapa" onclick="addAnexo(event, <?= $etapa['id'] ?>)">Adicionar Anexo</button>
                </div>
            </div>
            <?php foreach ($anexos as $key => $anexo) : ?>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mt-2 mb-2">
                    <?php if (in_array($anexo['tipo_anexo'], App\Models\ProjetoAnexoModel::TIPO_IMG)) : ?>
                        <img src="<?= site_url('projeto/api/etapa/readanexo/' . $etapa['projeto_id'] . '/' . $anexo['anexo']) ?>" class="img-thumbnail" alt="<?= $anexo['titulo'] ?>" onclick="openImg(event, <?= $anexo['projeto_id'] ?>,<?= $anexo['id'] ?>)" style="cursor:pointer;">
                    <?php else : ?>
                        <img src="<?= base_url('assets/images/pdf-icon.jpg') ?>" class="img-thumbnail" alt="<?= $anexo['titulo'] ?>" onclick="openPdf(event, <?= $anexo['projeto_id'] ?>,<?= $anexo['id'] ?>)" data-anexo="<?= $anexo['anexo'] ?>" style="cursor:pointer;">
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="tab-pane" id="etapa_comentario">
        <section class="row mt-4">
            <div class="col-md-12">
                <div class="bg-light mb-4 p-4">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex flex-start mb-2">
                            <img class="rounded-circle shadow-1-strong me-3" src="<?= base_url('assets/images/users/1.jpg') ?>" alt="avatar" width="65" height="65" />
                            <div class="mt-3">
                                <h5>Johny Cash</h5>
                                <p class="small">3 hours ago</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center">
                            <button class="btn btn-sm btn-outline-primary me-2"><i class="fa fa-trash"></i></button>
                            <button class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i></button>
                        </div>
                    </div>
                    <div>
                        <p class="text-justify">
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                            ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempusaasas
                            viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.dsdsdsd
                            Donec lacinia congue felis in faucibus ras purus odio, vestibulum in
                            vulputate at, tempus viverra turpis.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="bg-light mb-4 p-4">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex flex-start mb-2">
                            <img class="rounded-circle shadow-1-strong me-3" src="<?= base_url('assets/images/users/1.jpg') ?>" alt="avatar" width="65" height="65" />
                            <div class="mt-3">
                                <h5>Johny Cash</h5>
                                <p class="small">3 hours ago</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center">
                            <button class="btn btn-sm btn-outline-primary me-2"><i class="fa fa-trash"></i></button>
                            <button class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i></button>
                        </div>
                    </div>
                    <div>
                        <p class="text-justify">
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>