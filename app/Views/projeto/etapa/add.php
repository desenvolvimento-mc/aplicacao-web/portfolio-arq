<?php
$situacoes = \App\Models\ProjetoEtapaModel::SITUACAO;
?>


<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-md-5 align-self-center">
    <h3 class="text-themecolor">Adicionar Etapa</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?= site_url('projeto') ?>">Projetos</a></li>
        <li class="breadcrumb-item"><a href="<?= site_url('projeto/'.$projeto['id'].'/etapa') ?>">Etapas</a></li>
        <li class="breadcrumb-item active">Adicionar</li>
    </ol>
</div>
<div class="col-md-7 align-self-center">
    <a href="<?= site_url('projeto/'.$projeto['id'].'/etapa') ?>" class="btn btn-secondary pull-right">
        Etapas
    </a>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-7">
        <div class="card">
            <div class="card-body">
                <?= form_open('projeto/'. $projeto['id'] .'/etapa/add') ?>
                <form action="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="projeto_nome">Projeto</label>
                                <input type="text" class="form-control" id="projeto_nome" value="<?= $projeto['titulo'] ?>" disabled>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="cliente_nome">Cliente</label>
                                <input type="text" class="form-control" id="cliente_nome" value="<?= $projeto['cliente_nome'] ?>" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cliente_nome">CPF</label>
                                <input type="text" class="form-control" id="cliente_nome" value="<?= App\Helpers\hString::cpfCnpjFriendly($projeto['cliente_cpf']) ?>" disabled>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nome">Título da Etapa</label>
                                <input type="text" class="form-control <?= validation_show_error('nome') ? 'is-invalid' : '' ?>" id="nome" name="nome" value="<?= set_value('nome') ?>" required>
                                <small class="text-danger"><?= validation_show_error('nome') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="descricao">Descrição da Etapa</label>
                                <textarea class="form-control <?= validation_show_error('descricao') ? 'is-invalid' : '' ?>" name="descricao" id="descricao" required><?= set_value('descricao') ?></textarea>
                                <small class="text-danger"><?= validation_show_error('descricao') ?></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="percentual">Percentual</label>
                                <div class="input-group">
                                    <input type="number" min="1" max="100" class="form-control <?= validation_show_error('percentual') ? 'is-invalid' : '' ?>" id="percentual" name="percentual" value="<?= set_value('percentual') ?>" required>
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                                <small class="text-danger"><?= validation_show_error('percentual') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="situacao">Situação</label>
                                <select class="form-select" name="situacao" id="situacao" required>
                                    <?php foreach ($situacoes as $key => $situacao) : ?>
                                        <option value="<?= $key ?>"><?= $situacao ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="projeto_id" value="<?= $projeto['id'] ?>">
                    <button type="submit" class="btn btn-secondary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>