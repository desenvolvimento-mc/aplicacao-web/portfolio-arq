<?php
$badge = [
    1 => 'primary',
    2 => 'success'
];
?>

<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-md-5 align-self-center">
    <h3 class="text-themecolor">Etapas do Projeto </h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?= site_url('projeto') ?>">Projetos</a></li>
        <li class="breadcrumb-item active">Etapas</li>
    </ol>
</div>
<div class="col-md-7 align-self-center">
    <div class="btn-group pull-right" role="group" aria-label="Basic example" id="btn-group">
        <a href="<?= site_url('projeto') ?>" class="btn btn-outline-primary">Listar</a>
        <?php if ($projeto['situacao'] != 3) : ?>
            <a href="<?= site_url('projeto/' . $projeto['id'] . '/etapa/add') ?>" class="btn btn-outline-primary" id="new-etapa">Adicionar</a>
        <?php endif; ?>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-md-12">
        <?php if (isset($etapas) and !empty($etapas)) : ?>
            <div class="list-group">
                <?php foreach ($etapas as $key => $etapa) : ?>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" onclick="showEtapa(event, <?= $etapa['id'] ?>);" id="etapa_<?= $etapa['id'] ?>">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><?= $etapa['etapa_nome'] ?></h5>
                            <span class="text-muted font-12">
                                <?= $etapa['date_time_diff'] ?>
                                (<span class="badge badge-<?= $badge[$etapa['etapa_situacao']]?>" id="situacao"><?= App\Models\ProjetoEtapaModel::SITUACAO[$etapa['etapa_situacao']] ?></span>)
                            </span>
                        </div>
                        <small class="text-muted font-12">Projeto <?= $etapa['etapa_percentual'] ?>% concluído</small>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script src="<?= base_url('assets/js/sistema/etapa.js') ?>"></script>

<script class="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {
        const base_url = '<?= site_url('projeto/api/etapa') ?>';

        showEtapa = async (ev, id) => {
            ev.preventDefault();
            const etapa = new window.Etapa(base_url, id);
            let res = await etapa.show(ev);
            if (res) {
                console.log(res);
            }
        }

    });
</script>
<?= $this->endSection() ?>