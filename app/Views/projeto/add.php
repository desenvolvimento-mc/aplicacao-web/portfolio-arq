<?php
$situacoes = \App\Models\ProjetoModel::SITUACAO;
?>

<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-7 align-self-center">
    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Projetos</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0 p-0">
                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= site_url('projeto') ?>">Projetos</a></li>
                <li class="breadcrumb-item active">Adicionar</li>
            </ol>
        </nav>
    </div>
</div>
<div class="col-5 align-self-center">
    <div class="customize-input float-right">
        <a href="<?= site_url('projeto') ?>" class="btn btn-rounded btn-secondary pull-right">
            Listar
        </a>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?= form_open('projeto/add') ?>
                <div class="row">
                    <div class="col-md-7 row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control <?= validation_show_error('titulo') ? 'is-invalid' : '' ?>" id="titulo" name="titulo" value="<?= set_value('titulo') ?>" required>
                                <small class="text-danger"><?= validation_show_error('titulo') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control <?= validation_show_error('endereco') ? 'is-invalid' : '' ?>" name="endereco" id="endereco" value="<?= set_value('endereco') ?>" required>
                                <small class="text-danger"><?= validation_show_error('endereco') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="descricao">Descrição do Projeto</label>
                                <textarea class="form-control <?= validation_show_error('descricao') ? 'is-invalid' : '' ?>" name="descricao" id="descricao" required><?= set_value('descricao') ?></textarea>
                                <small class="text-danger"><?= validation_show_error('descricao') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prazo_medio">Prazo Médio</label>
                                <input type="text" class="form-control <?= validation_show_error('prazo_medio') ? 'is-invalid' : '' ?>" name="prazo_medio" id="prazo-medio" value="<?= set_value('prazo_medio') ?>" required>
                                <small class="text-danger"><?= validation_show_error('prazo_medio') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prazo_maximo">Prazo Máximo</label>
                                <input type="text" class="form-control <?= validation_show_error('prazo_maximo') ? 'is-invalid' : '' ?>" name="prazo_maximo" id="prazo-maximo" value="<?= set_value('prazo_maximo') ?>" required>
                                <small class="text-danger"><?= validation_show_error('prazo_maximo') ?></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 border-left">
                        <label for="clinte">Clientes</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="cpf" minlength="11" maxlength="11" placeholder="Insira o cpf do cliente">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="searchCliente(event)">Adicionar</button>
                            </div>
                        </div>
                        <table class="table table-sm" id="clientes">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <button type="submit" class="btn btn-secondary">Salvar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {

        removeCliente = (ev, id) => {
            ev.preventDefault();
            let tr = document.querySelector('#tr_' + id);
            if (tr) tr.remove();
        }

        searchCliente = (ev) => {
            ev.preventDefault();

            let inputCpf = document.querySelector('#cpf');
            let tableCliente = document.querySelector('#clientes');
            let tbody = tableCliente.querySelector('tbody');
            let cpfLens = inputCpf.value.length;

            if (cpfLens == 11 || cpfLens == 14) {
                loading(1);
                fetch('<?= site_url('cliente/api/find?cpf=') ?>' + inputCpf.value)
                    .then(response => {
                        if (response.status == 200) {
                            response.json().then(dados => {
                                inputCpf.value = '';

                                if (!dados) return false;
                                if (!tbody.querySelector('#tr_' + dados.id)) {
                                    let tr = document.createElement('tr');
                                    tr.id = `tr_${dados.id}`;
                                    tr.innerHTML = `
                                        <td>${dados.cliente_nome}</td>
                                        <td>${dados.cliente_cpf}</td>
                                        <td>
                                            <a href="#" class="text-danger" onclick="removeCliente(event,${dados.id})">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </td>
                                        <input type="hidden" name="cliente_empresa_id[]" value="${dados.id}">
                                    `;
                                    tbody.appendChild(tr);
                                }
                            });
                        } else {
                            console.log('error');
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })
                    .finally(() => {
                        loading(0);
                    });
            }
        }
    });
</script>
<?= $this->endSection() ?>