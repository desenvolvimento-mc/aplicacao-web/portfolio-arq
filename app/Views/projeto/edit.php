<?php
$situacoes = \App\Models\ProjetoModel::SITUACAO;
?>

<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-7 align-self-center">
    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Projetos</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0 p-0">
                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= site_url('projeto') ?>">Projetos</a></li>
                <li class="breadcrumb-item active">Editar</li>
            </ol>
        </nav>
    </div>
</div>
<div class="col-5 align-self-center">
    <div class="customize-input float-right">
        <a href="<?= site_url('projeto') ?>" class="btn btn-rounded btn-secondary pull-right">
            Listar
        </a>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?= form_open('projeto/edit/' . $dados['id']) ?>
                <div class="row">
                    <div class="col-md-7 row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control <?= validation_show_error('titulo') ? 'is-invalid' : '' ?>" id="titulo" name="titulo" value="<?= $dados['titulo'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('titulo') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control <?= validation_show_error('endereco') ? 'is-invalid' : '' ?>" name="endereco" id="endereco" value="<?= $dados['endereco'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('endereco') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="descricao">Descrição do Projeto</label>
                                <textarea class="form-control <?= validation_show_error('descricao') ? 'is-invalid' : '' ?>" name="descricao" id="descricao" required><?= $dados['descricao'] ?></textarea>
                                <small class="text-danger"><?= validation_show_error('descricao') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prazo_medio">Prazo Médio</label>
                                <input type="text" class="form-control <?= validation_show_error('prazo_medio') ? 'is-invalid' : '' ?>" name="prazo_medio" id="prazo-medio" value="<?= $dados['prazo_medio'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('prazo_medio') ?></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prazo_maximo">Prazo Máximo</label>
                                <input type="text" class="form-control <?= validation_show_error('prazo_maximo') ? 'is-invalid' : '' ?>" name="prazo_maximo" id="prazo-maximo" value="<?= $dados['prazo_maximo'] ?>" required>
                                <small class="text-danger"><?= validation_show_error('prazo_maximo') ?></small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-2">
                                <label>Honorários</label>
                                <button class="btn btn-sm btn-outline-primary" onclick="addHonorario(event);">
                                    Adicionar
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-light">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Valor (R$)</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_honorario">
                                        <?php if (isset($dados['honorarios']) and is_array($dados['honorarios'])) : ?>
                                            <?php foreach ($dados['honorarios'] as $key => $honorario) : ?>
                                                <tr style="cursor: pointer;">
                                                    <td><?= $honorario['nome'] ?></td>
                                                    <td>R$ <?= $honorario['valor'] ?></td>
                                                    <td>
                                                        <a href="#" class="text-primary" onclick="editHonorario(event, <?= $honorario['id'] ?>);">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a href="#" class="text-danger ml-3" onclick="removeTR(event, <?= $honorario['id'] ?>);">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 border-left">
                        <label for="clinte">Clientes</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="cpf" minlength="11" maxlength="11" placeholder="Insira o cpf do cliente">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="searchCliente(event)">Adicionar</button>
                            </div>
                        </div>
                        <table class="table table-sm" id="clientes">
                            <tbody>
                                <?php foreach ($dados['clientes'] as $key => $cliente) : ?>
                                    <tr id="tr_<?= $cliente['cliente_empresa_id'] ?>">
                                        <td><?= $cliente['cliente_nome'] ?></td>
                                        <td><?= $cliente['cliente_cpf'] ?></td>
                                        <td>
                                            <a href="#" class="text-danger" onclick="removeCliente(event,<?= $cliente['cliente_empresa_id'] ?>)">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </td>
                                        <input type="hidden" name="cliente_empresa_id[]" value="<?= $cliente['cliente_empresa_id'] ?>">
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?= $dados['id'] ?>">
                <button type="submit" class="btn btn-secondary">Salvar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {

        let projeto_id = document.querySelector('input[name="id"]')?.value;

        removeCliente = (ev, id) => {
            ev.preventDefault();
            let tr = document.querySelector('#tr_' + id);
            if (tr) tr.remove();
        }

        searchCliente = (ev) => {
            ev.preventDefault();

            let inputCpf = document.querySelector('#cpf');
            let tableCliente = document.querySelector('#clientes');
            let tbody = tableCliente.querySelector('tbody');
            let cpfLens = inputCpf.value.length;

            if (cpfLens == 11 || cpfLens == 14) {
                loading(1);
                fetch('<?= site_url('cliente/api/find?cpf=') ?>' + inputCpf.value)
                    .then(response => {
                        if (response.status == 200) {
                            response.json().then(dados => {
                                inputCpf.value = '';

                                if (!dados) return false;
                                if (!tbody.querySelector('#tr_' + dados.id)) {
                                    let tr = document.createElement('tr');
                                    tr.id = `tr_${dados.id}`;
                                    tr.innerHTML = `
                                        <td>${dados.cliente_nome}</td>
                                        <td>${dados.cliente_cpf}</td>
                                        <td>
                                            <a href="#" class="text-danger" onclick="removeCliente(event,${dados.id})">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </td>
                                        <input type="hidden" name="cliente_empresa_id[]" value="${dados.id}">
                                    `;
                                    tbody.appendChild(tr);
                                }
                            });
                        } else {
                            console.log('error');
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })
                    .finally(() => {
                        loading(0);
                    });
            }
        }

        findHonorario = (id) => {
            loading(1);
            return fetch('<?= site_url('honorario/find/') ?>' + id)
                .then(response => {
                    if (response.status == 200) {
                        return response.json().then(dados => {
                            return dados;
                        });
                    } else {
                        console.log('error');
                    }
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => {
                    loading(0);
                });
        }

        saveHonorario = (formData) => {
            loading(1);
            return fetch('<?= site_url('honorario/save') ?>', {
                    method: 'POST',
                    body: formData
                })
                .then(response => {
                    if (response.status == 200) {
                        return response.json().then(dados => {
                            return dados;
                        });
                    } else {
                        console.log('error');
                    }
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => {
                    loading(0);
                });
        }

        removeTR = (ev, id) => {
            ev.preventDefault();
            let tr = ev.currentTarget.closest('tr');

            let formData = new FormData();
            formData.append('id', id);
            formData.append('excluido', 1);

            loading(1);
            fetch('<?= site_url('honorario/save') ?>', {
                    method: 'POST',
                    body: formData
                })
                .then(response => {
                    if (response.status == 200) {
                        tr.remove();
                    } else {
                        console.log('error');
                    }
                })
                .catch(error => {
                    console.log(error);
                })
                .finally(() => {
                    loading(0);
                });

        }

        editHonorario = async (ev, id) => {
            ev.preventDefault();

            let tr = ev.currentTarget.closest('tr');
            let td = tr.querySelectorAll('td');
            let input = tr.querySelectorAll('input[type="hidden"]');

            let honorario = await this.findHonorario(id);

            if (!honorario) return false;

            let template = document.createElement('form');
            template.classList = "row";
            template.innerHTML = `
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input id="nome" name="nome" class="form-control" value="${honorario.nome}">
                </div>
            </div>
            <div class="col-md-6">
                <label for="valor">Valor</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">R$</span>
                    </div>
                    <input id="valor" name="valor" class="form-control" value="${honorario.valor}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nome">Descrição</label>
                    <textarea class="form-control" name="descricao">${honorario.descricao ? honorario.descricao : ''}</textarea>
                </div>
            </div>
            <input type="hidden" name="id" value="${honorario.id}">`;

            let modal = new window.modal();
            modal.show({
                title: 'Editar Honorário',
                message: template,
                buttons: [{
                        value: 'Editar',
                        class: 'primary',
                        func: async () => {
                            let form = template;
                            let formData = new FormData(form);
                            formData.append('projeto_id', projeto_id);
                            let res = await this.saveHonorario(formData);
                            if (res) {
                                td[0].innerHTML = formData.get('nome');
                                td[1].innerHTML = 'R$ ' + formData.get('valor');
                            }
                            modal.hide();
                        }
                    },
                    {
                        value: 'Cancelar',
                        class: 'light'
                    }
                ]
            })
        }

        addHonorario = (ev) => {
            ev.preventDefault();
            let template = document.createElement('form');
            template.classList = "row";
            template.innerHTML = `
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input id="nome" name="nome" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <label for="valor">Valor</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">R$</span>
                    </div>
                    <input id="valor" name="valor" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nome">Descrição</label>
                    <textarea class="form-control" name="descricao"></textarea>
                </div>
            </div>`;

            let modal = new window.modal();
            modal.show({
                title: 'Adicionar Honorário',
                message: template,
                buttons: [{
                        value: 'Adicionar',
                        class: 'primary',
                        func: async () => {
                            let tbody_honorario = document.getElementById('tbody_honorario');

                            let form = template;
                            let formData = new FormData(form);
                            formData.append('projeto_id', projeto_id);

                            let id = await this.saveHonorario(formData);
                            if (id) {
                                let tr = document.createElement('tr');
                                tr.innerHTML = `
                                <td>${formData.get('nome')}</td>
                                <td>R$ ${formData.get('valor')}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-outline-primary" onclick="editHonorario(event, ${id});">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-outline-danger" onclick="removeTR(event);">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </td>`;

                                tbody_honorario.appendChild(tr);
                            }


                            modal.hide();
                        }
                    },
                    {
                        value: 'Cancelar',
                        class: 'sm btn-light'
                    }
                ]
            })
        }
    });
</script>
<?= $this->endSection() ?>