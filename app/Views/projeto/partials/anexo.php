<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive overflow-y-hidden table-card">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Tamanho</th>
                                <th>Cadastro</th>
                                <th>Autor</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($projeto['anexos'] as $key => $anexo) : ?>
                                <tr>
                                    <td onclick="showAnexo(event, <?= $anexo['id'] ?>, '<?= $anexo['anexo'] ?>', '<?= $anexo['tipo_anexo'] ?>')" style="cursor:pointer">
                                        <div class="d-flex align-items-center">
                                            <?php if (in_array($anexo['tipo_anexo'], App\Models\ProjetoAnexoModel::TIPO_IMG)) : ?>
                                                <div class="icon-shape icon-lg rounded-3 bg-primary-soft">
                                                    <i class="bi bi-file-image icon-lg text-primary"></i>
                                                </div>
                                            <?php else : ?>
                                                <div class="icon-shape icon-lg rounded-3 bg-danger-soft">
                                                    <i class="bi bi-file-text icon-lg text-danger"></i>
                                                </div>
                                            <?php endif; ?>
                                            <div class="ms-3">
                                                <h5 class="mb-0">
                                                    <a href="#" class="text-inherit"><?= $anexo['titulo'] ?></a>
                                                </h5>
                                            </div>
                                        </div>
                                    </td>
                                    <td>2.3MB</td>
                                    <td><?= $anexo['anexo_data_cadastro'] ?></td>
                                    <td>
                                        <img src="<?= base_url('assets/images/users/2.jpg') ?>" alt="Image" class="rounded-circle avatar avatar-sm" data-toggle="tooltip" data-placement="bottom" title="Sahid da Silva Mansur">
                                    </td>
                                    <td>
                                        <a href="<?= base_url('assets/images/users/2.jpg') ?>" class="btn btn-ghost btn-icon btn-sm rounded-circle" data-toggle="tooltip" data-placement="bottom" title="Fazer Download" download>
                                            <i class="fa fa-download"></i>
                                        </a>
                                        <a href="#!" class="btn btn-ghost btn-icon btn-sm rounded-circle" data-toggle="tooltip" data-placement="bottom" title="Excluir">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script class="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {
        showAnexo = async (ev, id, anexo, tipo) => {
            ev.preventDefault();
            let anexoModel = new window.modal({width: '600px'});

            let template = document.createElement(tipo == 'application/pdf' ? 'iframe' : 'img');
            template.src = `<?= site_url('projeto/api/etapa/readanexo/' . $projeto['id'] . '/') ?>${anexo}`;
            template.width = '565';
            if ( tipo == 'application/pdf' )
                template.height = '565';

            anexoModel.show({
                clickOut: true,
                message: template,
            });
        }

    });
</script>