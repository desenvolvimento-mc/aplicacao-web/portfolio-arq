<?php
$badges = [
    1 => 'primary',
    2 => 'success'
];
?>

<div class="row">
    <?php if (isset($projeto['etapas']) and !empty($projeto['etapas'])) : ?>
        <?php foreach ($projeto['etapas'] as $key => $etapa) : ?>
            <div class="col-md-3 mb-5" id="etapa_<?= $etapa['id'] ?>">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 h5" id="etapa_nome"><a href="#" class="text-inherit" onclick="showEtapa(event, <?= $etapa['id'] ?>);" id="etapa_<?= $etapa['id'] ?>"><?= $etapa['etapa_nome'] ?></a></h4>
                            <span class="text-muted fs-6"><span class="badge badge-<?= $badges[$etapa['etapa_situacao']] ?>-soft" id="situacao"><?= App\Models\ProjetoEtapaModel::SITUACAO[$etapa['etapa_situacao']] ?></span></span>
                        </div>
                        <div class="mt-3">
                            <div class="d-flex justify-content-between mb-2 fs-6"> <span>Percentual do Projeto</span>
                                <span id="percentual"><?= $etapa['etapa_percentual'] ?>%</span>
                            </div>
                            <div class="progress" style="height: 6px;">
                                <?php if ($etapa['etapa_percentual'] <= 25) : ?>
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: <?= $etapa['etapa_percentual'] ?>%;" aria-valuenow="<?= $etapa['etapa_percentual'] ?>" aria-valuemin="1" aria-valuemax="100"></div>
                                <?php endif; ?>
                                <?php if ($etapa['etapa_percentual'] > 25 and $etapa['etapa_percentual'] <= 50) : ?>
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $etapa['etapa_percentual'] ?>%;" aria-valuenow="<?= $etapa['etapa_percentual'] ?>" aria-valuemin="1" aria-valuemax="100"></div>
                                <?php endif; ?>
                                <?php if ($etapa['etapa_percentual'] > 50 and $etapa['etapa_percentual'] <= 75) : ?>
                                    <div class="progress-bar bg-info" role="progressbar" style="width: <?= $etapa['etapa_percentual'] ?>%;" aria-valuenow="<?= $etapa['etapa_percentual'] ?>" aria-valuemin="1" aria-valuemax="100"></div>
                                <?php endif; ?>
                                <?php if ($etapa['etapa_percentual'] > 75 and $etapa['etapa_percentual'] <= 100) : ?>
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?= $etapa['etapa_percentual'] ?>%;" aria-valuenow="<?= $etapa['etapa_percentual'] ?>" aria-valuemin="1" aria-valuemax="100"></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <div class="d-flex justify-content-between">
                            <div class="w-50 py-3 px-4">
                                <h6 class="mb-0 text-muted">Início</h6>
                                <p class="text-dark fs-6  mb-0"><?= $etapa['etapa_data_cadastro'] ?></p>
                            </div>
                            <div class="border-start w-50 py-3 px-4">
                                <?php if ( is_null($etapa['etapa_dt_finalizado']) ) : ?>
                                    <h6 class="mb-0 text-muted">Atulizada em:</h6>
                                    <p class="text-dark fs-6  mb-0"><?= $etapa['etapa_data_atualizado'] ?></p>
                                <?php else : ?>
                                    <h6 class="mb-0 text-muted">Fim</h6>
                                    <p class="text-dark fs-6  mb-0"><?= $etapa['etapa_data_finalizado'] ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if ($projeto['situacao'] != 3) : ?>
        <div class="col-xxl-3 col-xl-4 col-lg-6 col-md-6 col-sm-12 mb-5">
            <a href="<?= site_url('projeto/' . $projeto['id'] . '/etapa/add') ?>">
                <div class="card h-100 border border-2 shadow-none card-dashed-hover p-12">
                    <div class="card-body d-flex flex-column justify-content-center text-center">
                        <i class="fa fa-plus text-secondary mb-3 fs-2" aria-hidden="true"></i>
                    </div>
                </div>
            </a>
        </div>
    <?php endif; ?>
</div>

<script src="<?= base_url('sistema/js/etapa.js') ?>"></script>
<script class="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {
        const base_url = '<?= site_url('projeto/api/etapa') ?>';

        showEtapa = async (ev, id) => {
            ev.preventDefault();
            ev.stopPropagation();
            const etapa = new window.Etapa(base_url, id);
            let res = await etapa.show(ev);
            if (res) {
                console.log(res);
            }
        }

    });
</script>