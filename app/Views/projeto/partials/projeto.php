<style>
    .nav-lb-tab .nav-item .nav-link.active,
    .nav-lb-tab .nav-item .nav-link:hover {
        background-color: transparent;
        border-bottom: 2px solid #000;
        color: #000;
    }
</style>

<div class="row">
    <div class="col-md-12 col-xl-8 col-12">
        <div class="row">
            <div class="col-12 mb-1">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Descrição</h4>
                    </div>
                    <div class="card-body">
                        <p align="justify"><?= $projeto['descricao'] ?></p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item px-0">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <h5 class="mb-0 ">Endereço</h5>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <p class="text-dark mb-0"><?= $projeto['endereco'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item px-0">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <h5 class="mb-0 ">Início</h5>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <p class="text-dark mb-0"><?= $projeto['data_cadastro'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item  px-0">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <h5 class="mb-0 ">Prazo Estimado</h5>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <p class="text-dark mb-0"><?= $projeto['prazo_maximo'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Honorários</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive overflow-y-hidden table-card">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Plano</th>
                                        <th>Valor</th>
                                        <th>Cadastro</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($projeto['honorarios'] as $key => $honorario) : ?>
                                        <tr>
                                            <td><?= $honorario['nome'] ?></td>
                                            <td>R$ <?= $honorario['valor'] ?></td>
                                            <td><?= $honorario['data_cadastro'] ?></td>
                                            <td>
                                                <button class="btn btn-sm btn-outline-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xl-4 col-12">

        <div class="card mb-5 bg-warning">
            <div class="card-body">
                <h4 class="mb-0 text-white">Tempo em Desenvolvimento</h4>
                <h3 class="display-5 fw-bold text-white mb-1"><?= $projeto['date_time_diff'] ?></h3>
            </div>
        </div>

        <div class="card mb-5 ">
            <div class="card-header">
                <h4 class=" mb-0 ">Percentual Concluído</h4>
            </div>
            <div class="card-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?= $projeto['percentual'] ?>%;" aria-valuenow="<?= $projeto['percentual'] ?>" aria-valuemin="0" aria-valuemax="100"><?= $projeto['percentual'] ?>%</div>
                </div>
                <!-- <canvas id="myChart"></canvas> -->
            </div>
        </div>
        <div class="card">

            <div class="card-header d-flex justify-content-between align-items-center">
                <div>
                    <h4 class="mb-0">Atividades Recentes</h4>
                </div>
                <div><a href="#!" class="btn btn-secondary btn-sm">Ver Todas</a></div>
            </div>

            <div class="card-body">
                <ul class="list-group list-group-flush ">
                    <li class="list-group-item px-0 pt-0 border-0 pb-6">
                        <div class="row position-relative">
                            <div class="col-auto">
                                <div class="icon-shape icon-md bg-primary-soft text-primary rounded-circle">
                                    <i data-feather="check" class="icon-xs"></i>
                                </div>
                            </div>
                            <div class="col ms-n2">
                                <h4 class="mb-0 h5">Etapa Finalizada</h4>
                                <p class="mb-0 ">Maria Clara finalizou a etapa 1</p>

                            </div>
                            <div class="col-auto">
                                <span class="text-muted fs-6">2 mins atrás</span>

                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 pt-0 border-0 pb-6">
                        <div class="row position-relative">
                            <div class="col-auto">
                                <div class="icon-shape icon-md bg-primary-soft text-primary rounded-circle line-icon">
                                    <i data-feather="message-square" class="icon-xs"></i>
                                </div>
                            </div>
                            <div class="col ms-n2">
                                <h4 class="mb-0 h5">Novo Comentário</h4>
                                <p class="mb-0 ">Joca deixou um comentário na etapa 1</p>

                            </div>
                            <div class="col-auto">
                                <span class="text-muted fs-6">1 hora atrás</span>

                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 pt-0 border-0 pb-6">
                        <div class="row position-relative">
                            <div class="col-auto">
                                <div class="icon-shape icon-md bg-primary-soft text-primary rounded-circle line-icon">
                                    <i data-feather="alert-triangle" class="icon-xs"></i>
                                </div>
                            </div>
                            <div class="col ms-n2">
                                <h4 class="mb-0 h5">Task Overdue</h4>
                                <p class="mb-0 ">Task <a href="#!"><u>status updatd for board</u></a>
                                    is overdue.</p>

                            </div>
                            <div class="col-auto">
                                <span class="text-muted fs-6">1 day</span>

                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 pt-0 border-0">
                        <div class="row position-relative">
                            <div class="col-auto">
                                <div class="icon-shape icon-md bg-primary-soft text-primary rounded-circle line-icon">
                                    <i data-feather="mail" class="icon-xs"></i>
                                </div>
                            </div>
                            <div class="col ms-n2">
                                <h4 class="mb-0 h5">Update Send to Client</h4>
                                <p class="mb-0 ">Jitu send email to update design
                                    for client Dash UI.</p>

                            </div>
                            <div class="col-auto">
                                <span class="text-muted fs-6">1 day</span>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>