<?= $this->extend('template/layouts/admin') ?>

<?= $this->section('breadcrumb') ?>
<div class="col-7 align-self-center">
    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Projetos</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0 p-0">
                <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
                <li class="breadcrumb-item text-muted active" aria-current="page">Projetos</li>
            </ol>
        </nav>
    </div>
</div>
<div class="col-5 align-self-center">
    <div class="customize-input float-right">
        <a href="<?= site_url('projeto/add') ?>" class="btn btn-rounded btn-secondary pull-right">
            Adicionar
        </a>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Prazo</th>
                                <th>Situação</th>
                                <th>Cadastro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($projetos as $key => $dados) : ?>
                                <tr>
                                    <td><?= $dados['projeto_titulo'] ?></td>
                                    <td><?= $dados['prazo_maximo'] ?></td>
                                    <td><?= $dados['percentual'] ?>% Concluído</td>
                                    <td><?= $dados['data_cadastro'] ?></td>
                                    <td>
                                        <a href="<?= site_url('projeto/'.$dados['id'].'/etapa') ?>" class="btn btn-sm btn-outline-primary">
                                            <i class="fa fa-clipboard"></i>
                                        </a>
                                        <a href="<?= site_url('projeto/edit/' . $dados['id']) ?>" class="btn btn-sm btn-outline-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button class="btn btn-sm btn-outline-danger" onclick="deleteId(<?= $dados['id'] ?>)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>