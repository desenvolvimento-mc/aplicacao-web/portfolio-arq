<?= $this->extend('template/layouts/login') ?>

<?= $this->section('content') ?>
<div class="auth-box row">
    <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url(assets/images/big/3.jpg);"></div>
    <div class="col-lg-5 col-md-7 bg-white">
        <div class="p-3">
            <div class="text-center">
                <img src="assets/images/big/icon.png" alt="wrapkit">
            </div>
            <h2 class="mt-3 text-center">Login</h2>
            <p class="text-center">Entre com as suas credenciais para acessar o sistema.</p>
            <?= $this->include('template/flash') ?>
            <?= form_open('login') ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="text-dark" for="email">Email</label>
                        <input class="form-control" name="email" id="email" type="text">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="text-dark" for="senha">Senha</label>
                        <input class="form-control" name="senha" id="senha" type="password">
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <button type="submit" class="btn btn-block btn-dark">Entrar</button>
                </div>
                <div class="col-lg-12 text-center mt-5">
                    Esqueceu a senha? <a href="#" class="text-danger">Recupere aqui!</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>