<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
// $routes->setDefaultController('Home');
// $routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index', ['filter' => 'authFilter:login']);
$routes->get('logout', 'Usuario\Login::logout');

// -- Agrupamento das rotas do login
$routes->group('login', static function ($routes) {
    $routes->get('/',  'Usuario\Login::index');
    $routes->post('/', 'Usuario\Login::login');
});

// -- Agrupamento das rotas do usuário
$routes->group('usuario',['filter' => 'authFilter:login'], static function ($routes) {
    $routes->get('', 'Usuario\Controller::index');

    $routes->get('add', 'Usuario\Controller::add');
    $routes->post('add', 'Usuario\Controller::save');

    $routes->get('edit/(:num)', 'Usuario\Controller::edit/$1');
    $routes->post('edit/(:num)', 'Usuario\Controller::save');
});

// -- Agrupamento das rotas do cliente
$routes->group('cliente',['filter' => 'authFilter:login'], static function ($routes) {
    $routes->get('', 'Cliente\Controller::index');

    $routes->get('add', 'Cliente\Controller::add');
    $routes->post('add', 'Cliente\Controller::save');

    $routes->get('edit/(:num)', 'Cliente\Controller::edit/$1');
    $routes->post('edit/(:num)', 'Cliente\Controller::save');

    $routes->get('api/find', 'Cliente\Api\Controller::findcliente');
});

// -- Agrupamento das rotas do projeto
$routes->group('projeto',['filter' => 'authFilter:login'], static function ($routes) {
    $routes->get('', 'Projeto\Controller::index');

    $routes->get('add', 'Projeto\Controller::add');
    $routes->post('add', 'Projeto\Controller::save');

    $routes->get('edit/(:num)', 'Projeto\Controller::edit/$1');
    $routes->post('edit/(:num)', 'Projeto\Controller::save');

    $routes->get('(:num)/etapa', 'Projeto\Etapa::index/$1');
    $routes->post('(:num)/etapa/add', 'Projeto\Etapa::save');
    $routes->get('(:num)/etapa/add', 'Projeto\Etapa::add/$1');

    $routes->post('api/etapa/save', 'Projeto\Api\Etapa::save');
    $routes->get('api/etapa/info/(:any)', 'Projeto\Api\Etapa::info/$1');
    $routes->post('api/etapa/insertanexo/(:any)', 'Projeto\Api\Etapa::insertanexo/$1');
    $routes->get('api/etapa/readanexo/(:any)/(:any)', 'Projeto\Api\Etapa::readfile/$1/$2');
    $routes->delete('api/etapa/deleteanexo/(:any)/(:any)', 'Projeto\Api\Etapa::deleteanexo/$1/$2');
});

// -- Agrupamento das rotas do honorario
$routes->group('honorario', static function ($routes) {
    $routes->get('find/(:num)', 'Projeto\Honorario::find/$1');
    $routes->post('save', 'Projeto\Honorario::save');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
