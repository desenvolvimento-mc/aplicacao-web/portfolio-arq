<?php

namespace App\Filters;

use App\Auth;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthFilter implements FilterInterface
{

    public function before(RequestInterface $request, $arguments = [])
    {
        $permissao = null;
        $redirect = 'login';

        if ( isset($arguments) and count($arguments) ) {
            foreach ($arguments as $key => $value) {
                if ( is_numeric($value) ) $permissao[] = $value;
                else $redirect = $value;
            }
        }

        if (!Auth::acesso($permissao)) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Acesso negado!']);
            return redirect()->to(site_url($redirect));
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        
    }


}
