<?php

namespace App\Helpers;

class hString
{

    // -- Formata os campos cpf e cnpj. Inserindo pontos e traços
    static public function cpfCnpjFriendly($str)
    {
        while (strlen($str) < 11) {
            $str = $str . "0";
        }

        while (strlen($str) > 11 and strlen($str) < 14) {
            $str = $str . "0";
        }

        switch (strlen($str)) {
            case 11:
                $first  = substr_replace($str, '.', 3, 0);
                $second = substr_replace($first, '.', 7, 0);
                return substr_replace($second, '-', 11, 0);
                break;

            case 14:
                $first  = substr_replace($str, '.', 2, 0);
                $second = substr_replace($first, '.', 6, 0);
                $third  = substr_replace($second, '/', 10, 0);
                return substr_replace($third, '-', 15, 0);
                break;

            default:
                return $str;
                break;
        }
    }

    // -- Recupera apenas os números da variável
    static public function onlyNumber($str)
    {
        return preg_replace('/[^0-9]/', '', $str);
    }

    // -- Verifica se o cpf é válido de acordo com o cálculo de validação do cpf
    static public function verifyCpf($cpf)
    {
        // Extrai somente os números
        $cpf = hString::onlyNumber($cpf);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }


}
