<?php

namespace App\Controllers\Cliente;

use App\Controllers\BaseController;
use App\Models\ClienteEmpresaModel;
use App\Models\ClienteModel;

class Controller extends BaseController
{

    private $Cliente;
    private $ClienteEmpresa;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->Cliente = new ClienteModel();
        $this->ClienteEmpresa = new ClienteEmpresaModel();
    }

    public function index()
    {
        $dados = $this->ClienteEmpresa->prepare()->findAll();
        return view('cliente/index', ['clientes' => $dados]);
    }

    public function add()
    {
        return view('cliente/add');
    }

    public function edit($id)
    {
        $dados = $this->ClienteEmpresa->prepare()->where('cliente_empresa.id',$id)->first();
        if ( !$dados ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Cliente não encontrado!']);
            return redirect()->to('cliente');
        }
        return view('cliente/edit', ['dados' => $dados]);
    }

    public function save()
    {
        if (!$this->request->is('post')) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados!']);
            return redirect()->to('cliente');
        }
        
        $redirect = 'cliente/add';

        $data = $this->request->getPost();
        if (isset($data['id'])) $redirect = 'cliente/edit/' . $data['id'];

        $valid_data = $this->validateData(
            $data,
            $this->Cliente->validationRules,
            $this->Cliente->validationMessage
        );

        if ( !$valid_data ) {
            return redirect()->to($redirect)->withInput();
        }

        $res = $this->Cliente->saveCliente($data);

        if ($res) {
            session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Cliente salvo com sucesso!']);
            return redirect()->to('cliente')->withInput();
        } else {
            return redirect()->to($redirect)->withInput();
        }
    }

}
