<?php

namespace App\Controllers\Cliente\Api;

use App\Controllers\BaseController;
use App\Models\ClienteEmpresaModel;

class Controller extends BaseController
{
    private $ClienteEmpresa;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->ClienteEmpresa = new ClienteEmpresaModel();
    }

    public function findcliente()
    {
        if ( $this->request->is('get') ) {
            $clienteCpf = $this->request->getGet('cpf');
            if ( $clienteCpf ) {
                $dados = $this->ClienteEmpresa->prepare()->where('cliente.cpf', $clienteCpf)->first();
                return $this->response->setJSON($dados ?: false);
            }
        }
        return $this->response->setJSON(false);
    }
}
