<?php

namespace App\Controllers\Usuario;

use App\Auth;
use App\Controllers\BaseController;
use App\Models\UsuarioModel;

class Controller extends BaseController
{

    private $Usuario;
    private $userAuth;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->Usuario  = new UsuarioModel();
        $this->userAuth = Auth::getData();
    }

    public function index()
    {
        $dados = $this->Usuario->prepare()->where('usuario.empresa_id', $this->userAuth['empresa_id'])->findAll();
        return view('usuario/index', ['usuarios' => $dados]);
    }

    public function add()
    {
        return view('usuario/add');
    }

    public function edit($id)
    {
        $dados = $this->Usuario->where('usuario.id',$id)->where('usuario.empresa_id', $this->userAuth['empresa_id'])->first();
        if ( !$dados ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Usuário não encontrado!']);
            return redirect()->to('usuario');
        }
        return view('usuario/edit', ['dados' => $dados]);
    }

    public function save()
    {
        if ( !$this->request->is('post') ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados!']);
            return redirect()->to('usuario');
        }

        $data = $this->request->getPost();
        $redirect = 'usuario/add';
        if ( isset($data['id']) ) $redirect = 'usuario/edit/' . $data['id'];

        $valid_data = $this->validateData(
            $data,
            $this->Usuario->validationRules,
            $this->Usuario->validationMessage
        );

        if ( !$valid_data ) {
            return redirect()->to($redirect)->withInput();
        }


        if ( $data['senha'] != $data['conf_senha'] ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Senhas não conferem!']);
            return redirect()->route($redirect)->withInput();
        }

        if ( $data['acesso'] == 1 ) $data['dt_final_acesso'] = null;

        $res = $this->Usuario->saveUsuario($data);

        if ( $res ) {
            session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Usuário salvo com sucesso!']);
            return redirect()->route('usuario')->withInput();
        } else {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Erro ao salvar os dados do usuário!']);
            return redirect()->to($redirect)->withInput();
        }
    }

}
