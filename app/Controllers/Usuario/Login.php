<?php

namespace App\Controllers\Usuario;

use App\Models\UsuarioModel;

use App\Auth;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;

class Login extends BaseController
{
    private $Usuario;

    use ResponseTrait;
    
    public function __construct()
    {
        helper('form');
        helper('url_helper');

        $this->Usuario = new UsuarioModel();
    }

    // 24-08-2023
    // -- Retorna a view de login
    public function index()
    {
        return view('login/index');
    }

    // 24-08-2023
    // -- Realiza o login do usuário no sistema
    public function login()
    {
        if ( !$this->request->is('post') ) {
            return redirect()->to('login');
        }

        $data = $this->request->getPost();

        $valid_data = $this->validateData(
            $data,
            [
                'email' => 'required',
                'senha' => 'required',
            ],
            [
                'email' => [
                    'required' => 'O Email é obrigatório'
                ],
                'senha' => [
                    'required' => 'A SENHA é obrigatório'
                ]
            ]
        );

        if ( !$valid_data ) {
            return redirect()->to('login')->withInput();
        }

        $result = $this->Usuario->login($data);

        if ( !$result ) {
            return redirect()->to('login')->withInput();
        } else {
            session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Sessão iniciada!']);
            return redirect()->to('/');
        }
    }

    // 24-08-2023
    // -- Realiza o logout do sistema
    public function logout()
    {
        if ( !Auth::getData() ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Usuário não encontrado!']);
            return redirect()->to('login');
        }

        Auth::logout();
        session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Sessão encerrada!']);
        return redirect()->to('login');
    }

    
}
