<?php

namespace App\Controllers\Projeto;

use App\Controllers\BaseController;
use App\Models\ClienteProjetoModel;
use App\Models\HonorarioModel;
use App\Models\ProjetoAnexoModel;
use App\Models\ProjetoEtapaModel;
use App\Models\ProjetoModel;

class Etapa extends BaseController
{
    private $Projeto;
    private $Honorario;
    private $ProjetoEtapa;
    private $ProjetoAnexo;
    private $ClienteProjeto;

    public function __construct()
    {
        helper('form');
        helper('url_helper');

        $this->Projeto        = new ProjetoModel();
        $this->Honorario      = new HonorarioModel();
        $this->ProjetoAnexo   = new ProjetoAnexoModel();
        $this->ProjetoEtapa   = new ProjetoEtapaModel();
        $this->ClienteProjeto = new ClienteProjetoModel();
    }

    public function index($id)
    {
        $findProjeto = $this->Projeto->prepare()->find($id);

        if ( !is_numeric($id) or !$findProjeto) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Projeto não encontrado na base de dados! Por favor, selecione um projeto existente.']);
            return redirect()->to('projeto');
        }

        $findProjeto['etapas']     = $this->ProjetoEtapa->prepare()->where('projeto_etapa.projeto_id',$id)->orderBy('projeto_etapa.dt_cadastro', 'DESC')->findAll();
        $findProjeto['anexos']     = $this->ProjetoAnexo->prepare()->where('projeto_anexo.projeto_id', $id)->findAll();
        $findProjeto['honorarios'] = $this->Honorario->prepare()->where('honorario.projeto_id', $id)->findAll();
        $findProjeto['clientes']   = $this->ClienteProjeto->prepare()->where('cliente_projeto.projeto_id', $id)->findAll();

        return view('projeto/info', [ 'projeto' =>$findProjeto]);
    }

    public function add($id)
    {
        $findProjeto = $this->Projeto->prepare()->find($id);

        if ( $findProjeto['situacao'] == 3 ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Projeto encontra-se finalizado!']);
            return redirect()->back();
        }

        if ( !is_numeric($id) or !$findProjeto) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Projeto não encontrado na base de dados! Por favor, selecione um projeto existente.']);
            return redirect()->to('projeto');
        }

        return view('projeto/etapa/add', ['projeto' => $findProjeto]);
    }

    public function save()
    {
        if ( !$this->request->is('post') ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados!']);
            return redirect()->to('projeto');
        }

        $data = $this->request->getPost();

        $valid_data = $this->validateData(
            $data,
            $this->ProjetoEtapa->validationRules,
            $this->ProjetoEtapa->validationMessage
        );

        if ( !$valid_data ) {
            return redirect()->back()->withInput();
        }

        $res = $this->ProjetoEtapa->saveEtapa($data);

        if ( $res['status'] ) {
            session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Etapa criada com sucesso!']);
            return redirect()->to('projeto/'.$data['projeto_id'].'/etapa');
        } else {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => isset($res['message']) ? $res['message'] : 'Erro ao salvar os dados da etapa do projeto!']);
            return redirect()->to('projeto/'.$data['projeto_id'].'/etapa/add')->withInput();
        }
    }

}
