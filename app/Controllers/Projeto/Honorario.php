<?php

namespace App\Controllers\Projeto;

use App\Controllers\BaseController;
use App\Models\HonorarioModel;

class Honorario extends BaseController
{
    private $Honorario;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->Honorario = new HonorarioModel();
    }

    public function find($id)
    {
        if ($this->request->is('get')) {
            $dados = $this->Honorario->prepare()->find($id);
            return $this->response->setJSON($dados ?: false);
        }
        return $this->response->setJSON(false);
    }

    public function save()
    {
        if ( $this->request->is('post') ) {
            $data = $this->request->getPost();
            $res = $this->Honorario->save($data);
            if ( !isset($data['id']) ) $res = $this->Honorario->getInsertID();
            return $this->response->setJSON($res);
        }
        return $this->response->setJSON(false);
    }
}
