<?php

namespace App\Controllers\Projeto;

use App\Controllers\BaseController;
use App\Models\ClienteProjetoModel;
use App\Models\HonorarioModel;
use App\Models\ProjetoModel;

class Controller extends BaseController
{

    private $Projeto;
    private $Honorario;
    private $ClienteProjeto;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->Projeto        = new ProjetoModel();
        $this->Honorario      = new HonorarioModel();
        $this->ClienteProjeto = new ClienteProjetoModel();
    }

    public function index()
    {
        $dados = $this->Projeto->prepare()->findAll();
        return view('projeto/index', ['projetos' => $dados]);
    }

    public function add()
    {
        return view('projeto/add');
    }

    public function edit($id)
    {

        $dados = $this->Projeto->prepare()->where('projeto.id',$id)->first();
        if ( !$dados ) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Projeto não encontrado!']);
            return redirect()->to('projeto');
        }

        $dados['honorarios'] = $this->Honorario
        ->where('honorario.projeto_id', $dados['id'])
        ->where('honorario.excluido', 0)
        ->findAll();

        $dados['clientes'] = $this->ClienteProjeto
        ->prepare()
        ->where('cliente_projeto.projeto_id', $dados['id'])
        ->findAll();
        
        return view('projeto/edit', ['dados' => $dados]);
    }

    public function save()
    {
        if (!$this->request->is('post')) {
            session()->setFlashdata('flash', ['class' => 'danger', 'message' => 'Não foi possível salvar os dados!']);
            return redirect()->to('projeto');
        }

        $data = $this->request->getPost();
        $redirect = 'projeto/add';
        if ( isset($data['id']) ) $redirect = 'projeto/edit/' . $data['id'];

        // $valid_data = $this->validateData(
        //     $data,
        //     $this->Projeto->validationRules,
        //     $this->Projeto->validationMessage
        // );

        // if ( !$valid_data ) {
        //     return redirect()->to($redirect)->withInput();
        // }
        
        $res = $this->Projeto->saveProjeto($data);

        if ($res) {
            session()->setFlashdata('flash', ['class' => 'success', 'message' => 'Projeto salvo com sucesso!']);
            return redirect()->to('projeto')->withInput();
        } else {
            return redirect()->to($redirect)->withInput();
        }
    }

}
