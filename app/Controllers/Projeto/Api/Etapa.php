<?php

namespace App\Controllers\Projeto\Api;

use App\Models\ProjetoModel;
use App\Models\ProjetoEtapaModel;
use App\Models\ProjetoAnexoModel;

use CodeIgniter\Files\File;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;

class Etapa extends BaseController
{
    use ResponseTrait;

    private $Projeto;
    private $ProjetoEtapa;
    private $ProjetoAnexo;

    public function __construct()
    {
        helper('form');
        helper('url');

        $this->Projeto      = new ProjetoModel();
        $this->ProjetoEtapa = new ProjetoEtapaModel();
        $this->ProjetoAnexo = new ProjetoAnexoModel();
    }

    // -- MC 21-08-2023
    // -- Retorna a view com as informações, anexos e comentários da etapa.
    public function info($id)
    {
        if ( !is_numeric($id) ) {
            return $this->fail('Requisição inválida!', 500);
        }

        $etapa = $this->ProjetoEtapa->prepare()->find($id);
        if ( !$etapa ) {
            return $this->fail('Etapa não encontrada!', 404);
        }

        $anexos = $this->ProjetoAnexo->prepare()->where('projeto_anexo.projeto_id', $etapa['projeto_id'])->where('projeto_anexo.projeto_etapa_id', $etapa['id'])->findAll();
        return $this->response->setJSON(view('projeto/etapa/api/info', ['etapa' => $etapa, 'anexos' => $anexos]));
    }

    // -- MC 21-08-2023
    // -- Salva os dados da etapa.
    public function save() 
    {
        if ( $this->request->is('post') ) {
            $data = $this->request->getPost();
            $res = $this->ProjetoEtapa->saveEtapa($data);
            return $res['status'] ? $this->respond(['message' => 'Etapa atualizada com sucesso!', 'projeto_situacao' => $res['projeto_situacao'] ], 200) : $this->fail(isset($res['message']) ? $res['message'] : 'Não foi possível atualizar a etapa!', 500);
        }
        return $this->fail('Requisição inválida!', 500);
    }

    // -- MC 21-08-2023
    // -- Insere um novo anexo.
    public function insertanexo($etapa_id) 
    {
        if ( $this->request->is('post') ) {

            $findEtapa = $this->ProjetoEtapa->find($etapa_id);

            if ( !is_numeric($etapa_id) or !$findEtapa) {
                return $this->fail('Etapa não encontrada!', 404);
            }

            $data = $this->request->getPost();
            $data['anexo'] = $this->request->getFile('anexo');
            $data['projeto_etapa_id'] = $etapa_id;
            $data['projeto_id'] = $findEtapa['projeto_id'];

            $res = $this->ProjetoAnexo->saveAnexo($data);
            return $res ? $this->respond(['message' => 'Anexo inserido com sucesso!'], 200) : $this->fail('Não foi possível inserir o anexo!', 500);
        }
        return $this->fail('Requisição inválida!', 500);
    }

    // -- MC 22-08-2023
    // -- Exclui uma imagem do projeto
    public function deleteanexo($projeto_id, $anexo_id)
    {
        if ( !is_numeric($projeto_id) or !is_numeric($anexo_id) ) {
            return $this->fail('Requisição inválida!', 500);
        }

        $anexo = $this->ProjetoAnexo->find($anexo_id);

        if ( !$this->Projeto->find($projeto_id) or !$anexo ) {
            return $this->fail('Projeto não encontrado na base de dados! Por favor, selecione um projeto válido', 404);
        }

        $path = WRITEPATH . 'projetos/'.$projeto_id.'/anexos/'.$anexo['anexo'];
		$fullpath = $path;

        if ( file_exists($fullpath) ) {
            return $this->ProjetoAnexo->deleteId($anexo_id) ? 
            $this->respond(['message' => 'Anexo deletado com sucesso!'], 200) :
            $this->fail('Não foi possível deletar o anexo da base de dados! Por favor, tente novamente mais tarde ou entre em contato com a administração do sistema.', 404); 
        }

        return true;
    }

    // -- MC 21-08-2023
    // -- Retorna a url dos anexos do projeto.
    public function readfile($projeto_id, $anexo)
	{
        if ( !is_numeric($projeto_id) or !$this->Projeto->find($projeto_id) ) {
            return $this->fail('Projeto não encontrado na base de dados! Por favor, selecione um projeto válido', 404);
        }

        $path = WRITEPATH . 'projetos/' . $projeto_id . '/anexos/' . $anexo;
        $fullpath = $path;

        if ( !file_exists($fullpath) ) $fullpath = '';

        $file = new File($fullpath, true);
        header('Content-Type: ' . $file->getMimeType());
        header('Content-Disposition: inline; filename=' . $file->getBasename());
        ob_clean();
        flush();

        return readfile($fullpath);
	}


}
