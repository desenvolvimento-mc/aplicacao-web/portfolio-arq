<?php

namespace App\Controllers\Empresa;

use App\Controllers\BaseController;
use App\Models\EmpresaModel;

class Controller extends BaseController
{
    public function index()
    {
        $Empresa = new EmpresaModel();
        $dados = $Empresa->prepare()->findAll();
        return view('empresa/index', ['empresas' => $dados]);
    }

    public function add()
    {

    }
    
}