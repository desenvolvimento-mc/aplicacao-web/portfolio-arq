const alert = {
	danger: {
		backgroundColor: '#f8d7da',
		borderColor: '#f1aeb5',
		color: '#58151c'
	},
	success: {
		backgroundColor: '#d1e7dd',
		borderColor: '#a3cfbb',
		color: '#0a3622'
	},
	info: {
		backgroundColor: '#d1ecf1',
		borderColor: '#bee5eb',
		color: '#0c5460'
	}
};

loading = function (bool) {
	var template = document.getElementById('loading');

	if (!template) {
		var css =
		`@keyframes spinner { 
			to {
				transform: rotate(360deg); 
			}
		}` +
		`#loading {
			position:fixed;
			z-index:9999;
			top:0;
			left:0;
			text-align:center;
			display:block;
			width:100%;
			height:100%;
			margin:0;
			border:0;
			padding:0;
			background-color:rgba(255,255,255,0.8);
		}` +
		`#loading:before {
			content: "";
			box-sizing: border-box;
			position: absolute;
			top: 50%;
			left: 50%;
			width: 60px;
			height: 60px;
			margin-top: -30px;
			margin-left: -30px;
			border-radius: 50%;
			border: 2px solid transparent;
			border-top-color: #a10304;
			border-bottom-color: #a10304;
			animation: spinner 0.7s ease infinite;
		}`;

		var head  = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement('style');
		if (style.styleSheet) {
			style.styleSheet.cssText = css;
		} else {
			style.appendChild(document.createTextNode(css));
		}
		head.appendChild(style);

		var template = document.createElement('div');
		template.id = "loading";
		template.style.display = 'none';
		document.body.appendChild(template);
	}

	if (bool) {
		template.style.display = 'block';
	} else {
		template.style.display = 'none';
	}
	return true;
}


// -- Gera a modal
window.modal = function Modal(attributes={}) {

	var $this = this;

	const attr = {
		width: '500px',
        ... attributes
    };

	let templateBackdrop = document.createElement('div');
	templateBackdrop.id = 'modalBackdrop';
	templateBackdrop.classList = 'modal-backdrop fade';
	templateBackdrop.style.display = 'none';

	let getBackdrop = document.querySelectorAll('#modalBackdrop');
	if ( getBackdrop.length ) {
		templateBackdrop.style.zIndex = '1055';
	}

	let getModalCss = document.getElementById('modal-css');

	if ( !getModalCss ) {
		var modalCss =
		`#closeX {
			position:absolute;
			z-index:1100; 
			top:4px; 
			right:6px; 
			height:30px; 
			width:30px; 
			border:0; 
			border-radius:15px; 
			color:rgba(68,68,68,0.61);
			font-size:14px; 
			font-weight:bold;
		}` + 
		`#closeX:hover{
			color:rgba(68,68,68,1);
		}`;
	
		let head  = document.head || document.getElementsByTagName('head')[0];
		let style = document.createElement('style');
		style.id = 'modal-css';
		if (style.styleSheet) {
			style.styleSheet.cssText = modalCss;
		} else {
			style.appendChild(document.createTextNode(modalCss));
		}
		head.appendChild(style);
	}

	let template = document.createElement('div');
	template.id = 'modal';
	template.classList = 'modal fade';
	template.setAttribute('tabindex', '-1');
	template.setAttribute('role', 'dialog');
	template.setAttribute('aria-labelledby', 'exampleModalLabel');
	template.setAttribute('aria-hidden', 'true');

	template.innerHTML = `
	<div class="modal-dialog " role="document" style="max-width:${attr.width};">
		<div class="modal-content">
			<button type="button" id="closeX" style="">X</button>
			<div class="modal-header">
				<h5 class="modal-title" id="modalLabel"></h5>
			</div>
			<div class="modal-body" id="modalMessage"></div>
			<div class="modal-footer" id="modalFooter"></div>
		</div>
	</div>`;
	
	document.body.appendChild(templateBackdrop);
	document.body.appendChild(template);

	var backdrop   = templateBackdrop;
	var div        = template;
	var divContent = div.querySelector('.modal-content');
	var bar        = div.querySelector('#modalLabel');
	var message    = div.querySelector('#modalMessage');
	var footer     = div.querySelector('#modalFooter');
	var closeX 	   = div.querySelector('#closeX');
	
	this.show = function (options) {

		closeX.onclick = $this.hide;

		if (options.buttons != undefined) {
			for (var i = 0; i < options.buttons.length; i++) {
				if (options.buttons[i].func == undefined) options.buttons[i].func = $this.hide;
				if (options.buttons[i].class == undefined) options.buttons[i].class = 'default';
			};
		} else {
			options.buttons = [];
			// options.buttons = [{ value: 'Fechar', class: 'light', func: function () { $this.hide() } }];
		}

		if ( options.title ) bar.innerHTML = options.title;
		else bar.parentNode.remove();
		
		// bar.innerHTML = (options.title) ? options.title : '';

		if ( options.preset !== undefined ) {
			let preset = alert[options.preset];
			if ( preset ) {
				divContent.style.color = preset.color;
				closeX.style.backgroundColor = 'transparent';
				divContent.style.backgroundColor = preset.backgroundColor;
			} else {
				divContent.style.backgroundColor = options.preset;
			}
			
			divContent.style.border = '0';
			divContent.style.boxShadow = 'none';
			
			if ( options.preset.background == 'transparent' ) message.style.padding = '0';
		}

		if ( options.clickOut ){
			div.addEventListener('click',async (ev) => {
				ev.stopPropagation();
				if (ev.target != div) return null;
				$this.hide();
			});
		}

		document.addEventListener('keydown', (ev) => {
			ev.stopPropagation();
			if (ev.key == "Escape") $this.hide();
		});
		

		if (options.message) {
			message.innerHTML = '';
			if (typeof options.message == 'object') message.appendChild(options.message);
			else if (typeof options.message == 'string') message.innerHTML = options.message;
			else message.innerHTML = 'Alerta de aviso!';
		}

		footer.innerHTML = '';
		for (var i = 0; i < options.buttons.length; i++) {
			var btn = document.createElement('button');

			btn.className = 'btn btn-sm btn-' + options.buttons[i].class;
			btn.type = 'button';
			btn.innerHTML = options.buttons[i].value;
			btn.onclick = options.buttons[i].func;

			footer.appendChild(btn);
		};

		if ( !footer.innerHTML ) footer.remove();
		
		div.style.display = 'block';
		backdrop.style.display = 'block';
		setTimeout(function () {
			backdrop.classList.add('show');
			div.classList.add('show');
		}, 200);
		
	}
	
	this.hide = function () {
		console.log('fechando modal');
		div.classList.remove('show');
		backdrop.classList.remove('show');
		setTimeout(function () {
			div.remove();
			backdrop.remove();
			// div.style.display = 'none';
			// backdrop.style.display = 'none';
		}, 600);
	}

	this.message = function () {
		return message;
	}

}