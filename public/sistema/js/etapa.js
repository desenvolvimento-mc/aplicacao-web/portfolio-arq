var base_url = null;
var etapa_id = null

function Etapa(url, id) {
    base_url = url;
    etapa_id = id;

    this.error = function (text) { console.log('error ' + text); }
    this.beforeSend = () => { loading(1) };
    this.afterSend = () => { loading(0) };
}

window.etapa = new Etapa();

var template = document.createElement('div');

etapaInfo = async () => {
    loading(1);
    return fetch(`${base_url}/info/${etapa_id}`).then(response => {
        if (response.status == 200) {
            return response.text().then(dados => {
                template.innerHTML = dados;
                return dados;
            });
        } else {
            response.json().then(error => {
                console.log(error);
                let errorModal = new window.modal();
                errorModal.show({
                    clickOut: true,
                    preset: 'danger',
                    message: error.messages.error
                });
            });
            return false;
        }
    }).catch(error => {
        console.log(error);
        let errorModal = new window.modal();
        errorModal.show({
            preset: 'danger',
            message: error
        });
    }).finally(() => {
        loading(0);
    });
}

request = async (url, options = {}) => {
    loading(1);
    let body = options.data ? options.data : null;

    return fetch(`${base_url}/${url}`, {
        method: options.method,
        body
    }).then(response => {
        if (response.status == 200) {
            return response.json().then(res => {
                let success = new window.modal();
                success.show({
                    preset: 'success',
                    clickOut: true,
                    message: res.message
                });
                return res;
            });
        } else {
            response.json().then(error => {
                console.log(error);
                let errorModal = new window.modal();
                errorModal.show({
                    clickOut: true,
                    preset: 'danger',
                    message: error.messages.error
                });
            });
            return false;
        }
    }).catch(error => {
        console.log(error);
        let errorModal = new window.modal();
        errorModal.show({
            preset: 'danger',
            message: error
        });
        return false;
    }).finally(() => {
        loading(0);
    });
}

editEtapa = (ev) => {
    loading(1);
    setTimeout(() => {
        loading(0);
    }, 500);

    let inputAll = template.querySelectorAll('input[disabled]');
    let descricao = template.querySelector('textarea[disabled]');
    let situacao = template.querySelector('select[disabled]');

    if (inputAll && descricao && situacao) {

        let div = ev.target.parentNode;
        let btnSave = document.createElement('button');
        btnSave.id = 'save-etapa';
        btnSave.classList = 'btn btn-sm btn-success';
        btnSave.innerHTML = 'Salvar Etapa';
        btnSave.type = 'button';
        btnSave.onclick = this.saveEtapa;
        ev.target.remove();
        div.appendChild(btnSave);

        for (let i = 0; i < inputAll.length; i++) {
            if (inputAll[i].id != 'dt_cadastro' && inputAll[i].id != 'dt_atualizado')
                inputAll[i].disabled = false;
        }
        descricao.disabled = false;
        situacao.disabled = false;
    }
}

saveEtapa = async (ev) => {
    let form = template.querySelector('form');
    let formData = new FormData(form);
    let res = await this.request(`save`, { method: 'post', data: formData });
    if (!res) return false;
    let etapa_id = formData.get('id');
    await this.etapaInfo();
    
    let etapa = document.querySelector('#etapa_'+etapa_id);
    if ( etapa ) {
        let situacaoDesc = {
            1: 'Em aberto',
            2: 'Finalizado'
        };
        etapa.querySelector('#etapa_nome').innerHTML = formData.get('nome');
        etapa.querySelector('#situacao').innerHTML = situacaoDesc[formData.get('situacao')];
        etapa.querySelector('#situacao').className = `badge badge-${formData.get('situacao') == 1 ? 'primary' : 'success' }-soft`;
        
        let percent = formData.get('percentual');
        etapa.querySelector('#percentual').innerHTML = `${percent}%`;
        let progress = etapa.querySelector('.progress');
        if ( percent <= 25 ) {
            progress.innerHTML = `<div class="progress-bar bg-danger" role="progressbar" style="width: ${percent}%;" aria-valuenow=" ${percent}" aria-valuemin="1" aria-valuemax="100"></div>`;
        } else if ( percent > 25 && percent <= 50 ) {
            progress.innerHTML = `<div class="progress-bar bg-warning" role="progressbar" style="width: ${percent}%;" aria-valuenow=" ${percent}" aria-valuemin="1" aria-valuemax="100"></div>`;
        } else if ( percent > 50 && percent <= 75 ) {
            progress.innerHTML = `<div class="progress-bar bg-info" role="progressbar" style="width: ${percent}%;" aria-valuenow=" ${percent}" aria-valuemin="1" aria-valuemax="100"></div>`;
        } else if ( percent > 75 && percent <= 100 ) {
            progress.innerHTML = `<div class="progress-bar bg-success" role="progressbar" style="width: ${percent}%;" aria-valuenow=" ${percent}" aria-valuemin="1" aria-valuemax="100"></div>`;
        }

        let btnAddEtapa = document.querySelector('#new-etapa');
        if ( res.projeto_situacao == 3 ) {
            btnAddEtapa?.remove();
        } else {
            if ( !btnAddEtapa ) {
                let divBtnGroup = document.querySelector('#btn-group');
                let aAddEtapa = document.createElement('a');
                aAddEtapa.href = window.location.href+'/add';
                aAddEtapa.innerHTML = 'Adicionar';
                aAddEtapa.classList = 'btn btn-outline-primary';
                aAddEtapa.id = 'new-etapa';
                divBtnGroup?.append(aAddEtapa);
            }
        }
    }

    return true;
}

openImg = (ev, projeto_id, anexo_id) => {
    let titulo = ev.target.alt;

    let img = new window.modal({ width: '540px' });
    img.show({
        title: titulo,
        clickOut: true,
        message: `<img src="${ev.target.src}" style="object-fit:cover;max-height:500px;width:500px;"/>`,
        buttons: [{
            value: 'Baixar',
            class: 'primary'
        },
        {
            value: 'Excluir',
            class: 'danger',
            func: async () => {
                let res = await this.request(`deleteanexo/${projeto_id}/${anexo_id}/13`, { method: 'delete' });
                if (res) {
                    img.hide();
                    await this.etapaInfo();
                }
            }
        }]
    });
}

openPdf = (ev, projeto_id, anexo_id) => {
    let titulo = ev.target.alt;
    let anexo = ev.target.dataset.anexo;

    let pdf = new window.modal({ width: '600px' });
    pdf.show({
        title: titulo,
        clickOut: true,
        message: `<iframe src="${base_url}/readanexo/${projeto_id}/${anexo}" style="width:100%; min-height:440px"></iframe>`,
        buttons: [{
            value: 'Excluir',
            class: 'danger',
            func: async () => {
                let res = await this.request(`deleteanexo/${projeto_id}/${anexo_id}/13`, { method: 'delete' });
                if (res) {
                    pdf.hide();
                    await this.etapaInfo();
                }
            }
        }]
    });
}

addAnexo = (ev) => {
    let form = document.createElement('form');
    form.classList = 'row';
    form.innerHTML = `
    <div class="col-md-12 mb-2">
        <label for="nome">Título</label>
        <input type="text" class="form-control" name="titulo">
    </div>
    <div class="col-md-12 mb-2">
        <label for="nome">Descrição</label>
        <textarea class="form-control" name="descricao"></textarea>
    </div>
    <div class="col-md-12 mb-2">
        <label for="nome">Anexo</label>
        <input type="file" class="form-control" name="anexo">
    </div>`;

    let insert = new window.modal();
    insert.show({
        title: 'Adicionar Anexo',
        message: form,
        buttons: [{
            value: 'Salvar',
            class: 'primary',
            func: async () => {
                let formData = new FormData(form);
                let res = await this.request(`insertanexo/${etapa_id}`, { method: 'POST', data: formData });
                if (res) {
                    insert.hide();
                    await this.etapaInfo();
                }
            }
        }]
    });
}

Etapa.prototype.show = async (ev) => {
    let etapa = await this.etapaInfo();
    if (!etapa) return false;
    let modal = new window.modal({ width: '800px' });
    return modal.show({
        message: template,
    });
}