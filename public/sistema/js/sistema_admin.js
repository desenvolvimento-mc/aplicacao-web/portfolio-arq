
//--LOADING HTML--------------------------------------------------------------//
//----------------------------------------------------------------------------//

divLoading = function(bool){
    var template = document.getElementById('divLoading');
    //--Criando elemento caso não exista
    if (!template){
        //--Adicionando CSS
        // console.log('criando CSS loading');
        var css =   '@keyframes spinner { to {transform: rotate(360deg); }}'+
                    '#divLoading {position:fixed; z-index:9999; top:0; left:0; text-align:center; display:block; width:100%; height:100%; margin:0; border:0; padding:0; background-color:rgba(255,255,255,0.8); }'+
                    '#divLoading:before { content: ""; box-sizing: border-box; position: absolute; top: 50%; left: 50%; width: 60px; height: 60px; margin-top: -30px; margin-left: -30px; border-radius: 50%; border: 2px solid transparent; border-top-color: coral; border-bottom-color: coral; animation: spinner 0.7s ease infinite;}';
        var head = document.head || document.getElementsByTagName('head')[0];
        var style = document.createElement('style');
        style.type = 'text/css';
        if (style.styleSheet){
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        head.appendChild(style);

        //--Adicionando Template
        // console.log('criando template loading');
        var template = document.createElement('div');
        // template.className = "divLoading";
        template.id = "divLoading";
        template.style.display = 'none';
        document.body.appendChild(template);
    }
    //--Exibindo ou escondendo
    if (bool){
        template.style.display = 'block';
    } else {
        template.style.display = 'none';
    }
    return true;
}



//--ALERT HTML----------------------------------------------------------------//
//----------------------------------------------------------------------------//

alertScreen = new function AlertScreen(){

        console.log('iniciando AlertScreen');
        var $this = this;

        var backdrop = document.querySelector('#alertScreenBackdrop');
        var div      = document.querySelector('#alertScreen');
        var bar      = div.querySelector('#alertScreenLabel');
        var message  = div.querySelector('#alertScreenMessage');
        var button   = div.querySelector('#alertScreenButton');

        // --Exibe alert
        this.show = function(options){

                if ( options.botoes != undefined ){
                        for (var i=0; i < options.botoes.length ; i++ ) {
                                if ( options.botoes[i].func  == undefined ) options.botoes[i].func  = $this.hide;
                                if ( options.botoes[i].class == undefined ) options.botoes[i].class = 'default';
                        };
                } else {
                        options.botoes = [{value:'OK',class:'',func: function(){ $this.hide() } }];
                }

                bar.innerHTML = (options.titulo) ? options.titulo : 'Alerta';

                if ( options.mensagem ){
                    message.innerHTML = '';
                    if      (typeof options.mensagem == 'object' ) message.appendChild(options.mensagem);
                    else if (typeof options.mensagem == 'string' ) message.innerHTML = options.mensagem;
                    else message.innerHTML = 'Alerta de aviso!';
                }

                button.innerHTML = '';
                for (var i=0; i < options.botoes.length ; i++ ) {
                        var bnt = document.createElement('button');

                        bnt.className = 'btn btn-' + options.botoes[i].class;
                        bnt.type      = 'button';
                        bnt.innerHTML = options.botoes[i].value;
                        bnt.onclick   = options.botoes[i].func;

                        button.appendChild(bnt);
                };

                // backdrop.style.height = window.innerHeight + 'px';
                div.style.display = 'block';
                backdrop.style.display = 'block';
                setTimeout(function(){
                    backdrop.classList.add('show');
                    div.classList.add('show');
                },200)

        }

        //--Esconde Alert
        this.hide = function(){
            div.classList.remove('show');
            backdrop.classList.remove('show');
            setTimeout(function(){
                div.style.display = 'none';
                backdrop.style.display = 'none';
            },600)
        }

        this.message = function(){
            return message;
        }

}

//--REQUISIÇÕES HTTP----------------------------------------------------------//
//----------------------------------------------------------------------------//
function Ajax() {

    this.httpRequest = null;

    this.error      = function(text,status){  console.log('error ' + text , status); }
    this.beforeSend = false;
    this.afterSend  = false;

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        this.httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            this.httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                this.httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
            }
        }
    }
}

Ajax.prototype.request = function(url,options){

    var $this = this;
    var httpRequestTime = null;

    var options = {
        // contentType: (options.contentType  == undefined ) ? 'application/x-www-form-urlencoded; charset=UTF-8' : options.contentType,
        method     : (options.method  == undefined ) ? 'POST' : options.method ,
        headers    : options.headers,
        async      : (options.async  == undefined ) ? true   : options.async ,
        data       : (options.data == undefined ) ? null : options.data ,
        dataType   : (options.dataType  == undefined ) ? 'json' : options.dataType ,
        beforeSend : (options.beforeSend == undefined ) ? $this.beforeSend : options.beforeSend,
        afterSend  : (options.afterSend == undefined ) ? $this.afterSend : options.afterSend,
        complete   : (options.complete == undefined ) ? null : options.complete,
        error      : (options.error == undefined ) ? $this.error : options.error,
        timeout    : (options.timeout == undefined ) ? 30000 : options.timeout,
    }

    if (!$this.httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    $this.httpRequest.onreadystatechange = function (){
        try {
            if (this.readyState === 4) {

                clearTimeout(httpRequestTime);
                var data = this.responseText;
                if (options.dataType == 'json' && data){
                    try {
                        data = JSON.parse(data);
                    } catch( e ){
                        options.error('JSON parse Error: ' + e,this.status);
                    }
                }

                if (this.status === 200) {
                    if ( options.complete != undefined ){
                        options.complete(data);
                    } else {
                        return data;
                    }
                } else {
                    options.error(data,this.status);
                }
                if ( options.afterSend ) options.afterSend();
            }
        }
        catch( e ) {
            clearTimeout(httpRequestTime);
            options.error('Caught Exception: ' + e,this.status);
        }
    }


    $this.httpRequest.open(options.method, url, options.async);
    if ( options.contentType ) $this.httpRequest.setRequestHeader('Content-Type', options.contentType);
    if ( options.beforeSend  ) options.beforeSend();
    $this.httpRequest.send(options.data);

    if ( options.timeout ){
        httpRequestTime = setTimeout(function(){
            $this.httpRequest.abort();
        },options.timeout);
    }
}

ajax        = new Ajax();
ajax.beforeSend = function(text,status){
    divLoading(1);
}
ajax.afterSend = function(text,status){
    divLoading(0);
}
ajax.error = function(text,status){
    divLoading(0);
    console.log(text,status);
    alertScreen.show({titulo:'Erro',mensagem:'Por favor, verifique sua conexão com a internet para atualizar os dados do sistema!'});
}



function EditTable(elem, url){

    var table_empresa  = document.querySelector(elem);
    var tbody          = table_empresa.querySelector('tbody');

    var _campo = null;
    var _type  = null;
    var _key   = null;

    var currentElementEditable = null;

    tbody.ondblclick = function(ev){
        if (ev.target.tagName == 'TD' && ev.target.getAttribute('edittable') ) editar(ev.target);
    }

    var editar = function(elem){

        _campo = elem.getAttribute('edittable');
        _type  = elem.getAttribute('edittable-type');
        _key   = elem.parentNode.getAttribute('edittable-key');

        console.log(_campo,_type,_key);

		if (_type == 'bollean' ){
			var valor = elem.innerHTML;
			atualizaCampoValor(_key,_campo,valor)
            .then( value => {
				elem.innerHTML = value;
			})
            .catch(msg => {
				alertScreen.show({titulo:'Aviso',mensagem:msg});
			});

        } else {
			if ( currentElementEditable !== null && currentElementEditable != elem ){
				cancel(currentElementEditable);
			}
			elem.contentEditable = 'true';
			elem.setAttribute('currentvalue',elem.innerHTML);
			elem.onkeydown = keypressObserver;
			elem.style.background='#e3d471';
			currentElementEditable = elem;
		}

    }

    var cancel = function(elem){
        var currentvalue = elem.getAttribute('currentvalue');
        if (currentvalue){
            elem.innerHTML = currentvalue
            elem.removeAttribute('currentvalue');
        }
        elem.contentEditable = 'false';
        elem.style.background='';
    }

    var keypressObserver = function(ev){
        var code = ev.keyCode;
        if (code == 13 || code == 27 ){
            ev.preventDefault();
            if (code == 13){
                confirm(this);
            } else {
                cancel(this);
            }
        }
    }

    var confirm = function(elem){
        var valor = elem.innerHTML;
        if (_type == 'number' || _type == 'decimal'){
            valor = parseFloat(valor);
            if ( isNaN(valor) || valor < 0){
                elem.style.background='#e37171';
                return;
            }
        }

		atualizaCampoValor(_key,_campo,valor)
        .then(value => {
			elem.innerHTML = value;
			elem.style.background='';
			elem.contentEditable = 'false';
			elem.removeAttribute('currentvalue');
			currentElementEditable = null;
		})
		.catch(msg => {
			elem.style.background='#e37171';
			alertScreen.show({titulo:'Aviso',mensagem:msg});
		});

    }


	var atualizaCampoValor = function(id,campo, valor){
        divLoading(1);
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify({id,campo,valor}),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then( response => {
            divLoading(0);
            if (response.status == 200) {
                return response.json()
            } else {
                return response.json().then(data => Promise.reject(data.message))
            }
        })
	}
}
